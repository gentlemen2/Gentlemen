﻿using System;
using System.Collections;
using UnityEngine;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;

// Token: 0x02000012 RID: 18
public class DiskMaster : MonoBehaviour
{
	// Token: 0x0600005E RID: 94 RVA: 0x00003DEC File Offset: 0x00001FEC
	private void Start()
	{
		this.allDisks = GameObject.FindGameObjectsWithTag("Disk");
		if (GameObject.Find("_Globals") != null)
		{
			Globals component = GameObject.Find("_Globals").GetComponent<Globals>();
			this.nrDisksSim = component.g_nrDisksSim;
			MonoBehaviour.print("Max. Discs: " + this.nrDisksSim);
		}
		if (this.allDisks.Length < this.nrDisksSim)
		{
			this.nrDisksSim = this.allDisks.Length;
		}
		this.hideDisks();
	}

	// Token: 0x0600005F RID: 95 RVA: 0x00003E7C File Offset: 0x0000207C
	private void Update()
	{
	}

	// Token: 0x06000060 RID: 96 RVA: 0x00003E80 File Offset: 0x00002080
	private void hideDisks()
	{
		foreach (GameObject gameObject in this.allDisks)
		{
			gameObject.GetComponent<DiskItem>().toggleDisk(false);
		}
		this.spawnDisk();
	}

	// Token: 0x06000061 RID: 97 RVA: 0x00003EC0 File Offset: 0x000020C0
	public void spawnDisk()
	{
		int num = Random.Range(0, this.allDisks.Length);
		DiskItem component = this.allDisks[num].GetComponent<DiskItem>();
		if (this.actNrDisks < this.nrDisksSim)
		{
			if (!component.enabled)
			{
				component.toggleDisk(true);
				this.actNrDisks++;
			}
			base.StartCoroutine(this.waitForDisk());
		}
	}

	// Token: 0x06000062 RID: 98 RVA: 0x00003F28 File Offset: 0x00002128
	public void requestDisk()
	{
		base.StartCoroutine(this.waitForDisk());
	}

	// Token: 0x06000063 RID: 99 RVA: 0x00003F38 File Offset: 0x00002138
	public void gotDisk()
	{
		base.StartCoroutine(this.waitForDisk());
	}

	// Token: 0x06000064 RID: 100 RVA: 0x00003F48 File Offset: 0x00002148
	public IEnumerator waitForDisk()
	{
		yield return new WaitForSeconds(this.respawnTime);
		this.spawnDisk();
		yield break;
	}

	// Token: 0x04000057 RID: 87
	public int nrDisksSim = 5;

	// Token: 0x04000058 RID: 88
	public int actNrDisks;

	// Token: 0x04000059 RID: 89
	private GameObject[] allDisks;

	// Token: 0x0400005A RID: 90
	public float respawnTime = 4f;
}
