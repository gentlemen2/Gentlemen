﻿using System;
using UnityEngine;

// Token: 0x0200000D RID: 13
public class GuiFPS : MonoBehaviour
{
	// Token: 0x06000041 RID: 65 RVA: 0x000032D0 File Offset: 0x000014D0
	private void Start()
	{
		if (!base.GetComponent<GUIText>())
		{
			Debug.Log("UtilityFramesPerSecond needs a GUIText component!");
			base.enabled = false;
			return;
		}
		this.timeleft = this.updateInterval;
	}

	// Token: 0x06000042 RID: 66 RVA: 0x0000330C File Offset: 0x0000150C
	private void Update()
	{
		this.timeleft -= Time.deltaTime;
		this.accum += Time.timeScale / Time.deltaTime;
		this.frames++;
		if ((double)this.timeleft <= 0.0)
		{
			float num = this.accum / (float)this.frames;
			string text = string.Format("{0:F2} FPS", num);
			base.GetComponent<GUIText>().text = text;
			if (num < 30f)
			{
				base.GetComponent<GUIText>().material.color = Color.yellow;
			}
			else if (num < 10f)
			{
				base.GetComponent<GUIText>().material.color = Color.red;
			}
			else
			{
				base.GetComponent<GUIText>().material.color = Color.green;
			}
			this.timeleft = this.updateInterval;
			this.accum = 0f;
			this.frames = 0;
		}
	}

	// Token: 0x04000036 RID: 54
	public float updateInterval = 0.5f;

	// Token: 0x04000037 RID: 55
	private float accum;

	// Token: 0x04000038 RID: 56
	private int frames;

	// Token: 0x04000039 RID: 57
	private float timeleft;
}
