﻿using System;
using UnityEngine;

// Token: 0x0200002C RID: 44
public class WarpliftDoor : MonoBehaviour
{
	// Token: 0x06000123 RID: 291 RVA: 0x0000AFD0 File Offset: 0x000091D0
	private void Start()
	{
	}

	// Token: 0x06000124 RID: 292 RVA: 0x0000AFD4 File Offset: 0x000091D4
	private void Update()
	{
	}

	// Token: 0x06000125 RID: 293 RVA: 0x0000AFD8 File Offset: 0x000091D8
	private void OnTriggerStay2D(Collider2D col)
	{
		if (col.name == "_headCollider")
		{
			Transform parent = col.transform.parent;
			int activate = parent.GetComponent<PlayerControls>().activate;
			bool killed = parent.GetComponent<PlayerLife>().killed;
			bool grounded = parent.GetComponent<PlayerControls>().grounded;
			if (activate != 0 && !killed && grounded)
			{
				int actFloor = base.transform.parent.GetComponent<Warplift>().actFloor;
				if (!base.transform.parent.GetComponent<Warplift>().isBusy)
				{
					if (this.doorID == actFloor)
					{
						base.transform.parent.GetComponent<Warplift>().enterLift(parent, base.transform, activate);
					}
					if (this.doorID != actFloor && activate == 1)
					{
						base.transform.parent.GetComponent<Warplift>().callLift(this.doorID);
					}
				}
			}
		}
	}

	// Token: 0x04000166 RID: 358
	public int doorID;

	// Token: 0x04000167 RID: 359
	public Animator anim;
}
