﻿using System;
using UnityEngine;

// Token: 0x02000004 RID: 4
public class OilyRigTower : MonoBehaviour
{
	// Token: 0x06000010 RID: 16 RVA: 0x00002488 File Offset: 0x00000688
	private void Start()
	{
		this.startYPos = base.transform.position.y;
		this.nextYGoal = base.transform.position.y + this.maxYDifference;
		base.GetComponent<AudioSource>().loop = true;
	}

	// Token: 0x06000011 RID: 17 RVA: 0x000024DC File Offset: 0x000006DC
	private void Update()
	{
		if (((base.transform.position.y < this.startYPos && this.direction == 1) || (base.transform.position.y > this.nextYGoal && this.direction == -1)) && this.isBusy)
		{
			base.GetComponent<AudioSource>().Stop();
			base.GetComponent<Rigidbody2D>().velocity = new Vector2(0f, 0f);
			this.isBusy = false;
		}
	}

	// Token: 0x06000012 RID: 18 RVA: 0x00002574 File Offset: 0x00000774
	public void switchOn()
	{
		this.gotoFloor();
	}

	// Token: 0x06000013 RID: 19 RVA: 0x0000257C File Offset: 0x0000077C
	public void gotoFloor()
	{
		if (base.GetComponent<Rigidbody2D>().velocity.y == 0f && !this.isBusy)
		{
			base.GetComponent<AudioSource>().Play();
			this.direction *= -1;
			MonoBehaviour.print("Bewegung auslösen: " + this.direction);
			base.GetComponent<Rigidbody2D>().velocity = new Vector2(0f, this.speed * (float)(-(float)this.direction));
			this.isBusy = true;
		}
	}

	// Token: 0x04000008 RID: 8
	private float startYPos;

	// Token: 0x04000009 RID: 9
	public int direction = 1;

	// Token: 0x0400000A RID: 10
	private float speed = 0.3f;

	// Token: 0x0400000B RID: 11
	private float maxYDifference = 3.8f;

	// Token: 0x0400000C RID: 12
	private float nextYGoal;

	// Token: 0x0400000D RID: 13
	private bool isBusy;
}
