﻿using System;
using UnityEngine;

// Token: 0x0200000C RID: 12
public class GuiAmmo : MonoBehaviour
{
	// Token: 0x0600003E RID: 62 RVA: 0x000031F8 File Offset: 0x000013F8
	private void Start()
	{
		this.mainScript = GameObject.Find("_MainScript").GetComponent<Main>();
		this.Ammos = new int[5];
		this.setAmmo();
	}

	// Token: 0x0600003F RID: 63 RVA: 0x00003224 File Offset: 0x00001424
	public void setAmmo()
	{
		base.GetComponent<GUIText>().text = "Ammo";
		for (int i = 1; i <= this.mainScript.anzPlayer; i++)
		{
			this.Ammos[i] = GameObject.Find("Player" + i).GetComponent<PlayerControls>().ammo;
			string text = " " + this.Ammos[i].ToString() + " ";
			GUIText guiText = base.GetComponent<GUIText>();
			guiText.text += text;
		}
	}

	// Token: 0x04000030 RID: 48
	private Main mainScript;

	// Token: 0x04000031 RID: 49
	public PlayerControls P1;

	// Token: 0x04000032 RID: 50
	public PlayerControls P2;

	// Token: 0x04000033 RID: 51
	public int P1Ammo;

	// Token: 0x04000034 RID: 52
	public int P2Ammo;

	// Token: 0x04000035 RID: 53
	public int[] Ammos;
}
