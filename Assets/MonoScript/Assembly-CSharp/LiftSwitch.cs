﻿using System;
using UnityEngine;

// Token: 0x02000028 RID: 40
public class LiftSwitch : MonoBehaviour
{
	// Token: 0x06000109 RID: 265 RVA: 0x0000A390 File Offset: 0x00008590
	private void Start()
	{
		if (this.FloorNr == 0)
		{
			MonoBehaviour.print("Warnung: FloorNr nicht gesetzt für: " + base.name);
		}
		if (this.Lift == null)
		{
			MonoBehaviour.print("Warnung: Lift nicht gesetzt für: " + base.name);
		}
	}

	// Token: 0x0600010A RID: 266 RVA: 0x0000A3E4 File Offset: 0x000085E4
	private void Update()
	{
	}

	// Token: 0x0600010B RID: 267 RVA: 0x0000A3E8 File Offset: 0x000085E8
	private void OnTriggerStay2D(Collider2D col)
	{
		if (col.gameObject.tag == "Player")
		{
			this.Lift.gameObject.GetComponent<Lift>().gotoFloor(this.FloorNr);
		}
	}

	// Token: 0x04000140 RID: 320
	public Lift Lift;

	// Token: 0x04000141 RID: 321
	public int FloorNr;
}
