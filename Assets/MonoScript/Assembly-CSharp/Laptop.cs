﻿using System;
using System.Collections;
using UnityEngine;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;

// Token: 0x02000013 RID: 19
public class Laptop : MonoBehaviour
{
	// Token: 0x06000066 RID: 102 RVA: 0x00003FBC File Offset: 0x000021BC
	private void Start()
	{
		this.spawnPos = base.transform.position;
		this.mainCam = GameObject.Find("_MainCam").GetComponent<Camera>();
		this.mainScript = GameObject.Find("_MainScript").GetComponent<Main>();
		this.H2.normal.textColor = Color.white;
		this.H2.alignment = TextAnchor.MiddleCenter;
		this.H2.fontStyle = FontStyle.Bold;
		this.H2.fontSize = 14;
		float orthographicSize = this.mainCam.orthographicSize;
		this.offset = new Vector2(0f, orthographicSize);
	}

	// Token: 0x06000067 RID: 103 RVA: 0x00004060 File Offset: 0x00002260
	private void Update()
	{
	}

	// Token: 0x06000068 RID: 104 RVA: 0x00004064 File Offset: 0x00002264
	private void OnTriggerEnter2D(Collider2D col)
	{
		if (col.tag == "Player" && !col.transform.root.GetComponent<PlayerLife>().killed && this.canBeCollected)
		{
			this.owned = true;
			base.StartCoroutine("calcValue");
			base.StopCoroutine("respawnLaptop");
			if (base.transform.parent != null)
			{
				this.actPlGames.toggleLaptop(false);
				base.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 1f);
			}
			this.actPlGames = col.transform.root.GetComponent<PlayerGames>();
			this.actPlGames.toggleLaptop(true);
			base.transform.parent = col.transform;
			base.transform.position = new Vector3(col.transform.position.x, col.transform.position.y + 0.5f, -1.3f);
			base.StartCoroutine("pauseCapturing");
			base.GetComponent<AudioSource>().Play();
		}
	}

	// Token: 0x06000069 RID: 105 RVA: 0x0000419C File Offset: 0x0000239C
	public void loseLaptop()
	{
		this.owned = false;
		base.transform.parent = null;
		base.transform.GetComponent<SpriteRenderer>().color = Color.white;
		base.transform.GetComponent<SpriteRenderer>().enabled = true;
		base.GetComponent<AudioSource>().Play();
		base.StartCoroutine("respawnLaptop");
	}

	// Token: 0x0600006A RID: 106 RVA: 0x000041FC File Offset: 0x000023FC
	public void throwLaptop(bool facingRight)
	{
		base.StopCoroutine("pauseCapturing");
		base.StopCoroutine("unRigidLaptop");
		this.canBeCollected = false;
		int num = 1;
		if (!facingRight)
		{
			num *= -1;
		}
		this.loseLaptop();
		base.transform.GetComponent<SpriteRenderer>().color = this.inactiveColor;
		base.gameObject.AddComponent<Rigidbody2D>();
		base.GetComponent<Rigidbody2D>().mass = 0.5f;
		base.GetComponent<Rigidbody2D>().fixedAngle = true;
		base.GetComponent<Rigidbody2D>().velocity = new Vector2((float)(15 * num), 5f);
		base.StartCoroutine("unRigidLaptop");
	}

	// Token: 0x0600006B RID: 107 RVA: 0x0000429C File Offset: 0x0000249C
	public IEnumerator unRigidLaptop()
	{
		yield return new WaitForSeconds(0.5f);
		base.GetComponent<Collider2D>().isTrigger = false;
		yield return new WaitForSeconds(0.5f);
		base.GetComponent<Collider2D>().isTrigger = false;
		base.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
		Object.Destroy(base.GetComponent<Rigidbody2D>());
		yield return new WaitForSeconds(0.1f);
		base.GetComponent<Collider2D>().enabled = true;
		base.GetComponent<Collider2D>().isTrigger = true;
		yield return new WaitForSeconds(1f);
		this.canBeCollected = true;
		base.GetComponent<Collider2D>().isTrigger = true;
		base.transform.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 1f);
		yield break;
	}

	// Token: 0x0600006C RID: 108 RVA: 0x000042B8 File Offset: 0x000024B8
	public IEnumerator pauseCapturing()
	{
		this.canBeCollected = false;
		base.transform.GetComponent<SpriteRenderer>().color = this.inactiveColor;
		yield return new WaitForSeconds(1.5f);
		base.transform.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 1f);
		this.canBeCollected = true;
		yield break;
	}

	// Token: 0x0600006D RID: 109 RVA: 0x000042D4 File Offset: 0x000024D4
	public IEnumerator respawnLaptop()
	{
		yield return new WaitForSeconds((float)this.respawnTime);
		if (base.transform.parent == null)
		{
			base.transform.position = this.spawnPos;
			base.GetComponent<AudioSource>().Play();
		}
		yield break;
	}

	// Token: 0x0600006E RID: 110 RVA: 0x000042F0 File Offset: 0x000024F0
	private IEnumerator calcValue()
	{
		yield return new WaitForSeconds(0.1f);
		if (this.owned)
		{
			if (this.afterPoint < 1f)
			{
				this.afterPoint = 9f;
			}
			else
			{
				this.afterPoint -= 1f;
			}
		}
		else
		{
			base.StopCoroutine("calcValue");
		}
		base.StartCoroutine("calcValue");
		yield break;
	}

	// Token: 0x0600006F RID: 111 RVA: 0x0000430C File Offset: 0x0000250C
	private void OnGUI()
	{
		if (base.transform.parent != null)
		{
			float num = (float)(this.mainScript.gameGoals[1] - this.mainScript.Downloads[this.actPlGames.playerID]) * 1f;
			if (base.transform.GetComponent<SpriteRenderer>().enabled && num > 0f)
			{
				Vector3 vector = new Vector3(base.transform.position.x + this.offset.x, base.transform.position.y + this.offset.y, base.transform.position.z);
				Vector3 vector2 = this.mainCam.WorldToScreenPoint(vector);
				string text = string.Empty;
				if (this.afterPoint == 0f || this.afterPoint == 10f)
				{
					text = num.ToString() + ".0";
				}
				else
				{
					text = (num - 1f + this.afterPoint / 10f).ToString();
				}
				GUI.Box(new Rect(vector2.x, (float)Screen.height * 1.5f - vector2.y, 50f, 20f), string.Empty);
				GUI.Label(new Rect(vector2.x, (float)Screen.height * 1.5f - vector2.y, 50f, 20f), text, this.H2);
				if (num < 11f)
				{
					this.H2.normal.textColor = Color.yellow;
				}
				else
				{
					this.H2.normal.textColor = Color.white;
				}
			}
		}
	}

	// Token: 0x0400005B RID: 91
	public bool canBeCollected = true;

	// Token: 0x0400005C RID: 92
	public bool owned;

	// Token: 0x0400005D RID: 93
	private float afterPoint = 10f;

	// Token: 0x0400005E RID: 94
	private PlayerGames actPlGames;

	// Token: 0x0400005F RID: 95
	private Main mainScript;

	// Token: 0x04000060 RID: 96
	public Camera mainCam;

	// Token: 0x04000061 RID: 97
	private Vector2 offset;

	// Token: 0x04000062 RID: 98
	private Vector2 spawnPos;

	// Token: 0x04000063 RID: 99
	public int respawnTime = 5;

	// Token: 0x04000064 RID: 100
	private GUIStyle H2 = new GUIStyle();

	// Token: 0x04000065 RID: 101
	private Color inactiveColor = new Color(0.5f, 0.5f, 0.5f, 1f);
}
