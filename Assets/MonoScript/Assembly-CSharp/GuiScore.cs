﻿using System;
using System.Collections;
using UnityEngine;

// Token: 0x0200000E RID: 14
public class GuiScore : MonoBehaviour
{
	// Token: 0x06000044 RID: 68 RVA: 0x00003418 File Offset: 0x00001618
	private void Start()
	{
		this.MyScores = new int[5];
		this.OldScores = new int[5];
		this.plColorStyles = new GUIStyle[5];
		this.plColorTxtStyles = new GUIStyle[5];
		this.scoreNameGUI = this.scoreNameOutput.GetComponent<GUIText>();
		this.mainScript = GameObject.Find("_MainScript").GetComponent<Main>();
		this.anzPlayer = this.mainScript.anzPlayer;
		this.gameMode = this.mainScript.gameMode;
		for (int i = 1; i <= this.anzPlayer; i++)
		{
			this.plColorTxtStyles[i] = new GUIStyle();
			this.plColorTxtStyles[i].normal.textColor = Color.white;
			this.plColorTxtStyles[i].alignment = TextAnchor.MiddleCenter;
			this.plColorTxtStyles[i].fontStyle = FontStyle.Bold;
			this.plColorTxtStyles[i].fontSize = 25;
			this.plColorTxtStyles[i].font = this.scoreFont;
		}
		this.modeName = new string[] { "Kills", "Downloads", "Disks", "Bombkills" };
		this.modeFlicker = new bool[]
		{
			true,
			default(bool),
			true,
			true
		};
		this.scoreNameGUI.text = this.modeName[this.gameMode];
	}

	// Token: 0x06000045 RID: 69 RVA: 0x00003574 File Offset: 0x00001774
	private void Update()
	{
	}

	// Token: 0x06000046 RID: 70 RVA: 0x00003578 File Offset: 0x00001778
	public void setScore(int[] Scores, int anzPlayer)
	{
		int num = -9999;
		int num2 = 0;
		this.MyScores = Scores;
		for (int i = 1; i <= anzPlayer; i++)
		{
			if (num < this.MyScores[i])
			{
				num = this.MyScores[i];
				num2 = i;
			}
			else if (num == this.MyScores[i])
			{
				num2 = 0;
			}
			this.plColorTxtStyles[i].normal.textColor = Color.white;
		}
		if (num2 != 0)
		{
			this.plColorTxtStyles[num2].normal.textColor = Color.yellow;
		}
		for (int j = 1; j <= anzPlayer; j++)
		{
			if (this.OldScores[j] != this.MyScores[j] && this.modeFlicker[this.gameMode])
			{
				base.StartCoroutine(this.flickerScore(j));
			}
			this.OldScores[j] = this.MyScores[j];
		}
	}

	// Token: 0x06000047 RID: 71 RVA: 0x00003660 File Offset: 0x00001860
	private void OnGUI()
	{
		this.InitStyles();
		for (int i = 1; i <= this.anzPlayer; i++)
		{
			GUI.Box(new Rect((float)Screen.width / 2.5f + (float)i * 45f, (float)Screen.height * 0.01f, 40f, 40f), string.Empty, this.plColorStyles[i]);
			GUI.Box(new Rect((float)Screen.width / 2.5f + (float)i * 45f, (float)Screen.height * 0.012f, 40f, 40f), this.MyScores[i].ToString(), this.plColorTxtStyles[i]);
		}
	}

	// Token: 0x06000048 RID: 72 RVA: 0x0000371C File Offset: 0x0000191C
	private IEnumerator flickerScore(int playerID)
	{
		Color actTxtColor = this.plColorTxtStyles[playerID].normal.textColor;
		for (int i = 0; i < 8; i++)
		{
			yield return new WaitForSeconds(0.3f);
			if (i % 2 == 0)
			{
				this.plColorTxtStyles[playerID].normal.textColor = Color.black;
			}
			else
			{
				this.plColorTxtStyles[playerID].normal.textColor = actTxtColor;
			}
		}
		yield break;
	}

	// Token: 0x06000049 RID: 73 RVA: 0x00003748 File Offset: 0x00001948
	private void InitStyles()
	{
		if (this.plColorStyles[4] == null)
		{
			for (int i = 1; i <= this.anzPlayer; i++)
			{
				this.plColorStyles[i] = new GUIStyle(GUI.skin.box);
				Color color = this.mainScript.plColors[i];
				color.a = 0.4f;
				this.plColorStyles[i].normal.background = this.MakeTex(2, 2, color);
			}
		}
	}

	// Token: 0x0600004A RID: 74 RVA: 0x000037D0 File Offset: 0x000019D0
	private Texture2D MakeTex(int width, int height, Color col)
	{
		Color[] array = new Color[width * height];
		for (int i = 0; i < array.Length; i++)
		{
			array[i] = col;
		}
		Texture2D texture2D = new Texture2D(width, height);
		texture2D.SetPixels(array);
		texture2D.Apply();
		return texture2D;
	}

	// Token: 0x0400003A RID: 58
	private Main mainScript;

	// Token: 0x0400003B RID: 59
	private int anzPlayer;

	// Token: 0x0400003C RID: 60
	private int gameMode;

	// Token: 0x0400003D RID: 61
	public Transform scoreNameOutput;

	// Token: 0x0400003E RID: 62
	private GUIText scoreNameGUI;

	// Token: 0x0400003F RID: 63
	private GUIStyle[] plColorStyles;

	// Token: 0x04000040 RID: 64
	private GUIStyle[] plColorTxtStyles;

	// Token: 0x04000041 RID: 65
	public Font scoreFont;

	// Token: 0x04000042 RID: 66
	private string[] modeName;

	// Token: 0x04000043 RID: 67
	private bool[] modeFlicker;

	// Token: 0x04000044 RID: 68
	private int[] MyScores;

	// Token: 0x04000045 RID: 69
	private int[] OldScores;
}
