﻿using System;
using UnityEngine;

// Token: 0x02000017 RID: 23
public class lightSiren : MonoBehaviour
{
	// Token: 0x06000086 RID: 134 RVA: 0x00004D80 File Offset: 0x00002F80
	private void Start()
	{
	}

	// Token: 0x06000087 RID: 135 RVA: 0x00004D84 File Offset: 0x00002F84
	private void Update()
	{
		base.transform.Rotate(this.speed * Time.deltaTime, 0f, 0f);
	}

	// Token: 0x04000087 RID: 135
	public float speed = 100f;
}
