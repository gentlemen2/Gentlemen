﻿using System;
using System.Collections;
using UnityEngine;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;

// Token: 0x02000020 RID: 32
public class PlayerGames : MonoBehaviour
{
	// Token: 0x060000CB RID: 203 RVA: 0x00008DD8 File Offset: 0x00006FD8
	private void Start()
	{
		this.mainScript = GameObject.Find("_MainScript").GetComponent<Main>();
		this.mainCam = GameObject.Find("_MainCam").GetComponent<Camera>();
		this.plCtrl = base.transform.root.GetComponent<PlayerControls>();
		this.plLife = base.transform.root.GetComponent<PlayerLife>();
		this.plTrans = base.transform.root.GetComponent<PlayerTransform>();
		this.modeThrowLT = this.mainScript.modeThrowLT;
		this.playerID = this.plCtrl.playerID;
		this.myDisk = base.transform.root.Find("Disk").GetComponent<SpriteRenderer>();
		this.changeDisks();
		this.H2.normal.textColor = Color.white;
		this.H2.alignment = TextAnchor.MiddleCenter;
		this.H2.fontStyle = FontStyle.Bold;
		this.H2.fontSize = 14;
		float orthographicSize = this.mainCam.orthographicSize;
		this.offset = new Vector2(-0.8f, orthographicSize * 1.05f);
	}

	// Token: 0x060000CC RID: 204 RVA: 0x00008EF8 File Offset: 0x000070F8
	private void Update()
	{
		if (this.hasLaptop)
		{
			this.plCtrl.canShoot = false;
			if (Input.GetButtonDown(this.plCtrl.fireKey) && Time.time > this.lapThrowTime && this.modeThrowLT)
			{
				this.lapThrowTime = Time.time + 2.5f;
				base.StartCoroutine(this.threwLTnowshoot());
				base.transform.root.Find("Laptop").GetComponent<Laptop>().throwLaptop(this.plCtrl.facingRight);
			}
		}
	}

	// Token: 0x060000CD RID: 205 RVA: 0x00008F94 File Offset: 0x00007194
	public IEnumerator loadData()
	{
		yield return new WaitForSeconds(1f);
		if (this.hasLaptop && !this.plLife.killed)
		{
			this.mainScript.setDownload(this.playerID);
			base.StartCoroutine("loadData");
		}
		yield break;
	}

	// Token: 0x060000CE RID: 206 RVA: 0x00008FB0 File Offset: 0x000071B0
	private IEnumerator threwLTnowshoot()
	{
		yield return new WaitForSeconds(0.3f);
		this.toggleLaptop(false);
		yield break;
	}

	// Token: 0x060000CF RID: 207 RVA: 0x00008FCC File Offset: 0x000071CC
	public void toggleLaptop(bool gotLaptop)
	{
		this.hasLaptop = gotLaptop;
		if (this.plCtrl.ammo > 0)
		{
			base.transform.root.Find("Arms").GetComponent<Animator>().SetBool("armed", !gotLaptop);
		}
		this.plCtrl.canShoot = !gotLaptop;
		base.StartCoroutine("loadData");
	}

	// Token: 0x060000D0 RID: 208 RVA: 0x00009034 File Offset: 0x00007234
	private void OnColliderEnter2D(Collision2D col)
	{
		if (col.gameObject.tag == "Player" && this.hasLaptop)
		{
			PlayerGames component = col.transform.GetComponent<PlayerGames>();
			bool flag = component.hasLaptop;
			if (component.hasLaptop != this.hasLaptop)
			{
				component.toggleLaptop(this.hasLaptop);
				this.toggleLaptop(component.hasLaptop);
			}
		}
	}

	// Token: 0x060000D1 RID: 209 RVA: 0x000090A4 File Offset: 0x000072A4
	public void changeDisks()
	{
		if (this.nrDisks == 0)
		{
			this.myDisk.enabled = false;
		}
		else
		{
			this.myDisk.enabled = true;
		}
	}

	// Token: 0x060000D2 RID: 210 RVA: 0x000090DC File Offset: 0x000072DC
	public void leaveDisk()
	{
		if (this.nrDisks > 0)
		{
			GameObject gameObject = Object.Instantiate(this.lostDisk, base.transform.position, Quaternion.Euler(new Vector3(0f, 0f, 0f))) as GameObject;
			DiskItem component = gameObject.GetComponent<DiskItem>();
			component.lifetime = 5f;
			component.nrDisks = this.nrDisks;
			GameObject gameObject2 = GameObject.Find("DiskMaster");
			gameObject.transform.parent = gameObject2.transform;
			this.nrDisks = 0;
			this.changeDisks();
		}
	}

	// Token: 0x060000D3 RID: 211 RVA: 0x00009174 File Offset: 0x00007374
	private void OnGUI()
	{
		if (this.nrDisks > 1 && !this.plTrans.isHiding)
		{
			Vector3 vector = new Vector3(base.transform.position.x + this.offset.x, base.transform.position.y + this.offset.y, base.transform.position.z);
			Vector3 vector2 = this.mainCam.WorldToScreenPoint(vector);
			GUI.Box(new Rect(vector2.x, (float)Screen.height * 1.5f - vector2.y, 25f, 20f), string.Empty);
			GUI.Label(new Rect(vector2.x, (float)Screen.height * 1.5f - vector2.y, 25f, 20f), this.nrDisks.ToString(), this.H2);
		}
	}

	// Token: 0x040000FE RID: 254
	public Main mainScript;

	// Token: 0x040000FF RID: 255
	public PlayerControls plCtrl;

	// Token: 0x04000100 RID: 256
	private PlayerLife plLife;

	// Token: 0x04000101 RID: 257
	private PlayerTransform plTrans;

	// Token: 0x04000102 RID: 258
	public int playerID;

	// Token: 0x04000103 RID: 259
	public SpriteRenderer myDisk;

	// Token: 0x04000104 RID: 260
	public GameObject lostDisk;

	// Token: 0x04000105 RID: 261
	private bool modeThrowLT = true;

	// Token: 0x04000106 RID: 262
	public bool hasLaptop;

	// Token: 0x04000107 RID: 263
	public bool hasBomb;

	// Token: 0x04000108 RID: 264
	public int downloads;

	// Token: 0x04000109 RID: 265
	public float lapThrowTime;

	// Token: 0x0400010A RID: 266
	public int nrDisks;

	// Token: 0x0400010B RID: 267
	public Camera mainCam;

	// Token: 0x0400010C RID: 268
	private Vector2 offset;

	// Token: 0x0400010D RID: 269
	private GUIStyle H2 = new GUIStyle();
}
