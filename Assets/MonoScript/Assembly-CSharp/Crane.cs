﻿using System;
using System.Collections;
using UnityEngine;

// Token: 0x0200000B RID: 11
public class Crane : MonoBehaviour
{
	// Token: 0x06000038 RID: 56 RVA: 0x0000307C File Offset: 0x0000127C
	private void Start()
	{
		this.startPos = base.transform.position;
		this.moveCrane();
	}

	// Token: 0x06000039 RID: 57 RVA: 0x0000309C File Offset: 0x0000129C
	private void Update()
	{
		if (base.transform.position.x > 26f && this.direction == 1f)
		{
			base.transform.position = new Vector2(-26f, base.transform.position.y);
		}
		if (base.transform.position.x < -26f && this.direction == -1f)
		{
			base.transform.position = new Vector2(26f, base.transform.position.y);
		}
	}

	// Token: 0x0600003A RID: 58 RVA: 0x00003160 File Offset: 0x00001360
	public void switchOn()
	{
		BarrelStand component = this.barrel.GetComponent<BarrelStand>();
		component.rollLifetime = this.lifetime;
		component.respawnTime = this.lifetime;
		component.releaseBarrel();
		base.StartCoroutine(this.stopCrane());
	}

	// Token: 0x0600003B RID: 59 RVA: 0x000031A4 File Offset: 0x000013A4
	public void moveCrane()
	{
		base.GetComponent<Rigidbody2D>().velocity = new Vector2(this.speed * this.direction, 0f);
	}

	// Token: 0x0600003C RID: 60 RVA: 0x000031D4 File Offset: 0x000013D4
	public IEnumerator stopCrane()
	{
		base.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
		base.transform.position = this.startPos;
		yield return new WaitForSeconds(this.lifetime);
		this.moveCrane();
		yield break;
	}

	// Token: 0x0400002A RID: 42
	public GameObject barrel;

	// Token: 0x0400002B RID: 43
	public float direction = 1f;

	// Token: 0x0400002C RID: 44
	public float speed = 5f;

	// Token: 0x0400002D RID: 45
	public float lifetime = 7f;

	// Token: 0x0400002E RID: 46
	private Vector2 startPos;

	// Token: 0x0400002F RID: 47
	private bool onLoose;
}
