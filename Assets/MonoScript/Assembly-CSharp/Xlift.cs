﻿using System;
using UnityEngine;

// Token: 0x0200002D RID: 45
public class Xlift : MonoBehaviour
{
	// Token: 0x06000127 RID: 295 RVA: 0x0000B0F4 File Offset: 0x000092F4
	private void Start()
	{
		if (base.transform.parent != null)
		{
			this.parentYDiff = base.transform.parent.position.y - base.transform.position.y;
		}
		this.startXPos = base.transform.position.x;
		this.nextXGoal = base.transform.position.x + this.maxXDifference * (float)this.startToRight;
		if (this.startToRight == -1)
		{
			float num = this.startXPos;
			this.startXPos = this.nextXGoal;
			this.nextXGoal = num;
		}
	}

	// Token: 0x06000128 RID: 296 RVA: 0x0000B1B4 File Offset: 0x000093B4
	private void Update()
	{
		if (((base.transform.position.x < this.startXPos && this.startToRight == 1) || (base.transform.position.x > this.nextXGoal && this.startToRight == -1)) && this.isBusy)
		{
			base.GetComponent<AudioSource>().Stop();
			base.GetComponent<Rigidbody2D>().velocity = new Vector2(0f, 0f);
			this.isBusy = false;
		}
		base.transform.position = new Vector2(base.transform.position.x, base.transform.parent.position.y - this.parentYDiff);
	}

	// Token: 0x06000129 RID: 297 RVA: 0x0000B294 File Offset: 0x00009494
	public void switchOn()
	{
		this.moveOn();
	}

	// Token: 0x0600012A RID: 298 RVA: 0x0000B29C File Offset: 0x0000949C
	public void moveOn()
	{
		if (base.GetComponent<Rigidbody2D>().velocity.x == 0f && !this.isBusy)
		{
			base.GetComponent<AudioSource>().Play();
			this.startToRight *= -1;
			base.GetComponent<Rigidbody2D>().velocity = new Vector2(this.speed * (float)(-(float)this.startToRight), 0f);
			this.isBusy = true;
		}
	}

	// Token: 0x04000168 RID: 360
	private float startXPos;

	// Token: 0x04000169 RID: 361
	public int startToRight = 1;

	// Token: 0x0400016A RID: 362
	public float speed = 0.3f;

	// Token: 0x0400016B RID: 363
	public float maxXDifference = 8f;

	// Token: 0x0400016C RID: 364
	private float nextXGoal;

	// Token: 0x0400016D RID: 365
	private bool isBusy;

	// Token: 0x0400016E RID: 366
	private float parentYDiff;
}
