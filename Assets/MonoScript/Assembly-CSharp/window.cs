﻿using System;
using UnityEngine;

// Token: 0x02000008 RID: 8
public class window : MonoBehaviour
{
	// Token: 0x0600001F RID: 31 RVA: 0x00002920 File Offset: 0x00000B20
	private void Start()
	{
		this.audioSources = new AudioSource[this.audioClips.Length];
		for (int i = 0; i < this.audioClips.Length; i++)
		{
			GameObject gameObject = new GameObject("AudioPlayer");
			gameObject.transform.parent = base.gameObject.transform;
			this.audioSources[i] = gameObject.AddComponent<AudioSource>() as AudioSource;
			this.audioSources[i].clip = this.audioClips[i];
		}
	}

	// Token: 0x06000020 RID: 32 RVA: 0x000029A8 File Offset: 0x00000BA8
	private void Update()
	{
	}

	// Token: 0x06000021 RID: 33 RVA: 0x000029AC File Offset: 0x00000BAC
	public void damageObject(float damage)
	{
		this.health -= damage;
		this.audioSources[0].Play();
		if (this.health <= 0f)
		{
			this.killObject();
		}
	}

	// Token: 0x06000022 RID: 34 RVA: 0x000029E0 File Offset: 0x00000BE0
	public void killObject()
	{
		this.audioSources[1].Play();
		base.transform.GetComponent<Collider2D>().enabled = false;
		base.transform.GetComponent<SpriteRenderer>().sprite = this.spriteBroken;
	}

	// Token: 0x04000017 RID: 23
	public float health = 50f;

	// Token: 0x04000018 RID: 24
	public Sprite spriteBroken;

	// Token: 0x04000019 RID: 25
	public AudioClip[] audioClips;

	// Token: 0x0400001A RID: 26
	private AudioSource[] audioSources;
}
