﻿using System;
using System.Collections;
using UnityEngine;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;

// Token: 0x02000011 RID: 17
public class DiskItem : MonoBehaviour
{
	// Token: 0x06000056 RID: 86 RVA: 0x00003B24 File Offset: 0x00001D24
	private void Start()
	{
		this.mainCam = GameObject.Find("_MainCam").GetComponent<Camera>();
		this.diskMaster = GameObject.Find("DiskMaster").GetComponent<DiskMaster>();
		this.H2.normal.textColor = Color.white;
		this.H2.alignment = TextAnchor.MiddleCenter;
		this.H2.fontStyle = FontStyle.Bold;
		this.H2.fontSize = 14;
		if (this.lifetime > 0f)
		{
			base.StartCoroutine(this.destroyDisk());
		}
		float orthographicSize = this.mainCam.orthographicSize;
		this.offset = new Vector2(0f, orthographicSize);
	}

	// Token: 0x06000057 RID: 87 RVA: 0x00003BD0 File Offset: 0x00001DD0
	private void Update()
	{
	}

	// Token: 0x06000058 RID: 88 RVA: 0x00003BD4 File Offset: 0x00001DD4
	private void OnTriggerEnter2D(Collider2D col)
	{
		MonoBehaviour.print(col.name);
		if (col.tag == "Player")
		{
			PlayerGames component = col.transform.root.GetComponent<PlayerGames>();
			PlayerLife component2 = col.transform.root.GetComponent<PlayerLife>();
			if (this.enabled && !component2.killed)
			{
				component.nrDisks += this.nrDisks;
				component.changeDisks();
				this.nrDisks = 1;
				this.toggleDisk(false);
				this.diskMaster.gotDisk();
				base.GetComponent<AudioSource>().Play();
			}
		}
	}

	// Token: 0x06000059 RID: 89 RVA: 0x00003C78 File Offset: 0x00001E78
	public void toggleDisk(bool makeVisible)
	{
		base.GetComponent<Collider2D>().enabled = makeVisible;
		base.GetComponent<SpriteRenderer>().enabled = makeVisible;
		this.enabled = makeVisible;
	}

	// Token: 0x0600005A RID: 90 RVA: 0x00003CA4 File Offset: 0x00001EA4
	public IEnumerator pauseCapturing()
	{
		base.GetComponent<Collider2D>().enabled = false;
		yield return new WaitForSeconds(1f);
		base.GetComponent<Collider2D>().enabled = true;
		yield break;
	}

	// Token: 0x0600005B RID: 91 RVA: 0x00003CC0 File Offset: 0x00001EC0
	public IEnumerator destroyDisk()
	{
		yield return new WaitForSeconds(this.lifetime);
		this.diskMaster.actNrDisks -= this.nrDisks;
		this.diskMaster.requestDisk();
		Object.Destroy(base.gameObject);
		yield break;
	}

	// Token: 0x0600005C RID: 92 RVA: 0x00003CDC File Offset: 0x00001EDC
	private void OnGUI()
	{
		if (this.nrDisks > 1)
		{
			Vector3 vector = new Vector3(base.transform.position.x + this.offset.x, base.transform.position.y + this.offset.y, base.transform.position.z);
			Vector3 vector2 = this.mainCam.WorldToScreenPoint(vector);
			GUI.Box(new Rect(vector2.x, (float)Screen.height * 1.5f - vector2.y, 25f, 20f), string.Empty);
			GUI.Label(new Rect(vector2.x, (float)Screen.height * 1.5f - vector2.y, 25f, 20f), this.nrDisks.ToString(), this.H2);
		}
	}

	// Token: 0x04000050 RID: 80
	public int nrDisks = 1;

	// Token: 0x04000051 RID: 81
	public new bool enabled = true;

	// Token: 0x04000052 RID: 82
	public float lifetime;

	// Token: 0x04000053 RID: 83
	public Camera mainCam;

	// Token: 0x04000054 RID: 84
	private Vector2 offset;

	// Token: 0x04000055 RID: 85
	private GUIStyle H2 = new GUIStyle();

	// Token: 0x04000056 RID: 86
	public DiskMaster diskMaster;
}
