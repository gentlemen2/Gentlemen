﻿using System;
using UnityEngine;

// Token: 0x02000026 RID: 38
public class Lift : MonoBehaviour
{
	// Token: 0x060000FB RID: 251 RVA: 0x00009D7C File Offset: 0x00007F7C
	private void Start()
	{
		this.MainScript = GameObject.Find("_MainScript").GetComponent<Main>();
		this.FloorHeight = this.MainScript.FloorHeight;
		if (this.actFloor == 0)
		{
			MonoBehaviour.print("Warnung: Etage nicht gesetzt von: " + base.name);
		}
		this.audioSources = new AudioSource[this.audioClips.Length];
		for (int i = 0; i < this.audioClips.Length; i++)
		{
			GameObject gameObject = new GameObject("AudioPlayer");
			gameObject.transform.parent = base.gameObject.transform;
			this.audioSources[i] = gameObject.AddComponent<AudioSource>() as AudioSource;
			this.audioSources[i].clip = this.audioClips[i];
		}
		this.audioSources[2].loop = true;
	}

	// Token: 0x060000FC RID: 252 RVA: 0x00009E58 File Offset: 0x00008058
	private void Update()
	{
		if (this.direction == 1)
		{
			if (base.transform.position.y > this.newY)
			{
				this.stopLift();
			}
		}
		else if (this.direction == -1 && base.transform.position.y < this.newY)
		{
			this.stopLift();
		}
	}

	// Token: 0x060000FD RID: 253 RVA: 0x00009ECC File Offset: 0x000080CC
	private void OnTriggerStay2D(Collider2D col)
	{
		if (col.gameObject.tag == "Player")
		{
			this.playerCtrl = col.gameObject.GetComponent<PlayerControls>();
			int activate = this.playerCtrl.activate;
			if (activate != 0)
			{
				if (activate < 0 && this.actFloor != this.minFloor)
				{
					int num = this.actFloor - 1;
					this.gotoFloor(num);
				}
				else if (activate > 0 && this.actFloor != this.maxFloor)
				{
					int num2 = this.actFloor + 1;
					this.gotoFloor(num2);
				}
			}
		}
	}

	// Token: 0x060000FE RID: 254 RVA: 0x00009F6C File Offset: 0x0000816C
	public void switchOn(float floorNr)
	{
		int num = Mathf.FloorToInt(floorNr);
		this.gotoFloor(num);
	}

	// Token: 0x060000FF RID: 255 RVA: 0x00009F88 File Offset: 0x00008188
	public void gotoFloor(int newFloor)
	{
		if (base.GetComponent<Rigidbody2D>().velocity.y == 0f && this.direction == 0 && this.actFloor != newFloor)
		{
			this.newY = base.transform.position.y + (float)(newFloor - this.actFloor) * this.FloorHeight;
			int num = newFloor - this.actFloor;
			int num2;
			if (num > 0)
			{
				num2 = 1;
			}
			else
			{
				num2 = -1;
			}
			base.GetComponent<Rigidbody2D>().velocity = new Vector2(0f, (float)(this.speed * num2));
			this.actFloor = newFloor;
			this.direction = num2;
			this.audioSources[2].Play();
		}
	}

	// Token: 0x06000100 RID: 256 RVA: 0x0000A048 File Offset: 0x00008248
	public void stopLift()
	{
		this.audioSources[0].Play();
		this.audioSources[2].Stop();
		base.GetComponent<Rigidbody2D>().velocity = new Vector2(0f, 0f);
		this.direction = 0;
	}

	// Token: 0x0400012B RID: 299
	public int speed = 2;

	// Token: 0x0400012C RID: 300
	public int actFloor;

	// Token: 0x0400012D RID: 301
	public int maxFloor;

	// Token: 0x0400012E RID: 302
	public int minFloor;

	// Token: 0x0400012F RID: 303
	private PlayerControls playerCtrl;

	// Token: 0x04000130 RID: 304
	private string getVertAxis;

	// Token: 0x04000131 RID: 305
	public Main MainScript;

	// Token: 0x04000132 RID: 306
	private float FloorHeight;

	// Token: 0x04000133 RID: 307
	private float newY;

	// Token: 0x04000134 RID: 308
	private int direction;

	// Token: 0x04000135 RID: 309
	public AudioClip[] audioClips;

	// Token: 0x04000136 RID: 310
	private AudioSource[] audioSources;
}
