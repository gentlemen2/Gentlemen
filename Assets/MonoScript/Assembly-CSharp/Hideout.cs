﻿using System;
using UnityEngine;

// Token: 0x02000003 RID: 3
public class Hideout : MonoBehaviour
{
	// Token: 0x06000009 RID: 9 RVA: 0x00002390 File Offset: 0x00000590
	private void Start()
	{
	}

	// Token: 0x0600000A RID: 10 RVA: 0x00002394 File Offset: 0x00000594
	private void Update()
	{
	}

	// Token: 0x0600000B RID: 11 RVA: 0x00002398 File Offset: 0x00000598
	private void OnTriggerEnter2D(Collider2D col)
	{
		if (col.tag == "Player")
		{
			if (col.GetComponent<PlayerControls>().activate == 1)
			{
				this.playerHide(col);
			}
			if (col.GetComponent<PlayerControls>().activate == 0)
			{
				this.playerShow(col);
			}
		}
	}

	// Token: 0x0600000C RID: 12 RVA: 0x000023EC File Offset: 0x000005EC
	private void OnTriggerExit2D(Collider2D col)
	{
		this.playerShow(col);
	}

	// Token: 0x0600000D RID: 13 RVA: 0x000023F8 File Offset: 0x000005F8
	private void playerHide(Collider2D col)
	{
		col.GetComponent<PlayerControls>().canMove = false;
		col.GetComponent<PlayerTransform>().hideColor();
		col.gameObject.layer = 11;
	}

	// Token: 0x0600000E RID: 14 RVA: 0x0000242C File Offset: 0x0000062C
	private void playerShow(Collider2D col)
	{
		col.GetComponent<PlayerControls>().canMove = true;
		col.GetComponent<PlayerTransform>().resetColor();
		col.gameObject.layer = 10;
	}
}
