﻿using System;
using System.Collections;
using UnityEngine;

// Token: 0x0200001F RID: 31
public class PlayerControls : MonoBehaviour
{
	// Token: 0x060000C4 RID: 196 RVA: 0x000088B0 File Offset: 0x00006AB0
	private void Start()
	{
		if (this.invertYaxis != -1)
		{
			this.invertYaxis = 1;
		}
		if (GameObject.Find("_Globals") != null)
		{
			Globals component = GameObject.Find("_Globals").GetComponent<Globals>();
			this.horizAxis = component.g_Ctrls[this.playerID, 0];
			this.vertAxis = component.g_Ctrls[this.playerID, 1];
			this.fireKey = component.g_Ctrls[this.playerID, 2];
			if (component.g_Ctrls[this.playerID, 4] == "yNeg")
			{
				this.invertYaxis = -1;
			}
		}
		this.plLife = base.transform.root.GetComponent<PlayerLife>();
		this.myGunScript = base.transform.root.Find("Arms").Find("Gun").GetComponent<Gun>();
		this.playerPos = new Vector2(base.transform.position.x, base.transform.position.y);
		this.startAmmo = this.ammo;
		this.anim.SetBool("alive", true);
		if (this.playerID < 1)
		{
			MonoBehaviour.print("Player-ID für " + base.name + " noch nicht vergeben");
		}
	}

	// Token: 0x060000C5 RID: 197 RVA: 0x00008A1C File Offset: 0x00006C1C
	private void Update()
	{
		this.grounded = Physics2D.Linecast(base.transform.position, this.groundCheck.position, 1 << LayerMask.NameToLayer("Ground"));
		if (this.grounded)
		{
			this.anim.SetTrigger("Land");
		}
		else
		{
			this.anim.SetTrigger("Falling");
		}
		if (!this.plLife.killed)
		{
			float vert = Input.GetAxisRaw(this.vertAxis) * (float)this.invertYaxis;
			float hoz = Input.GetAxisRaw(this.horizAxis);
			if (vert < -0.1f)
			{
				this.activate = -1;
				this.anim.SetFloat("Duck", Mathf.Abs(vert));
				this.animArms.SetFloat("Duck", Mathf.Abs(vert));
			}
			else if (vert > 0.1f)
			{
				this.activate = 1;
			}
			if (vert == 0f && this.activate != 0)
			{
				this.activate = 0;
				this.anim.SetFloat("Duck", 0f);
				this.animArms.SetFloat("Duck", 0f);
			}
			if (this.canMove)
			{
				if (vert > -0.1f && vert > -0.7f && !this.isRolling)
				{
					base.transform.position += base.transform.right * hoz * this.speed * Time.deltaTime;
				}
				this.anim.SetFloat("Speed", Mathf.Abs(hoz));
				this.animArms.SetFloat("Speed", Mathf.Abs(hoz));
				if (hoz > 0f && !this.facingRight)
				{
					this.Flip();
				}
				else if (hoz < 0f && this.facingRight)
				{
					this.Flip();
				}
				if (!this.isRolling && this.grounded)
				{
					if (vert < -0.1f && vert >= -0.8f && this.anim.GetFloat("Speed") == 1f)
					{
						// never triggers. unless on gamepad?
						this.isRolling = true;
						base.StartCoroutine(this.makeSlideRoll("Roll"));
						//base.StartCoroutine(this.makeSlideRoll("Slide"));
					}
					else if (vert < -0.8f && hoz != 0f && ((hoz < -0.5f && !this.facingRight) || (hoz > 0.5f && this.facingRight)))
					{

						this.canShoot = false;
						this.isRolling = true;
						this.anim.SetTrigger("Roll");
						base.StartCoroutine(this.makeSlideRoll("Roll"));
					}
				}
/*				if (vert > 0.1f && !isRolling && grounded)
				{
					GetComponent<Rigidbody2D>().AddForce(new Vector2(0, jumpForce));
				}*/
			}
		}
	}

	// Token: 0x060000C6 RID: 198 RVA: 0x00008D0C File Offset: 0x00006F0C
	public IEnumerator makeSlideRoll(string type)
	{
		base.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
		float veloc = 3500f;
		int direction = 1;
		if (!this.facingRight)
		{
			direction = -1;
		}
		base.GetComponent<Rigidbody2D>().AddForce(new Vector2(veloc * (float)direction, base.GetComponent<Rigidbody2D>().velocity.y));
		yield return new WaitForSeconds(0.6f);
		if (this.canMove)
		{
			this.canShoot = true;
		}
		this.isRolling = false;
		anim.ResetTrigger(type);
		yield break;
	}

	// Token: 0x060000C7 RID: 199 RVA: 0x00008D28 File Offset: 0x00006F28
	private void Flip()
	{
		base.GetComponent<Rigidbody2D>().velocity = new Vector2(0f, base.GetComponent<Rigidbody2D>().velocity.y);
		this.facingRight = !this.facingRight;
		Vector3 localScale = base.transform.localScale;
		localScale.x *= -1f;
		base.transform.localScale = localScale;
	}

	// Token: 0x060000C8 RID: 200 RVA: 0x00008D98 File Offset: 0x00006F98
/*	public void meleeAttack()
	{
		this.myGunScript.gunMelee();
	}*/

	// Token: 0x060000C9 RID: 201 RVA: 0x00008DA8 File Offset: 0x00006FA8
	public void animIsDucking()
	{
		this.animArms.SetTrigger("Roll");
	}

	// Token: 0x040000E8 RID: 232
	[HideInInspector]
	public bool facingRight = true;

	// Token: 0x040000E9 RID: 233
	[HideInInspector]
	public bool grounded;

	// Token: 0x040000EA RID: 234
	[HideInInspector]
	public bool canMove = true;

	// Token: 0x040000EB RID: 235
	[HideInInspector]
	public bool canShoot = true;

	// Token: 0x040000EC RID: 236
	[HideInInspector]
	public bool isRolling;

	// Token: 0x040000ED RID: 237
	public int activate;

	// Token: 0x040000EE RID: 238
	public int playerID;

	// Token: 0x040000EF RID: 239
	public Transform groundCheck;

	// Token: 0x040000F0 RID: 240
	private PlayerLife plLife;

	// Token: 0x040000F1 RID: 241
	public Animator anim;

	// Token: 0x040000F2 RID: 242
	public Gun myGunScript;

	// Token: 0x040000F3 RID: 243
	public Animator animArms;

	// Token: 0x040000F4 RID: 244
	public float speed = 1f;

	// Token: 0x040000F5 RID: 245
	public int ammo = 30;

	// Token: 0x040000F6 RID: 246
	public string horizAxis;

	// Token: 0x040000F7 RID: 247
	public string vertAxis;

	// Token: 0x040000F8 RID: 248
	public int invertYaxis;

	// Token: 0x040000F9 RID: 249
	public string fireKey;

	// Token: 0x040000FA RID: 250
	[HideInInspector]
	public Vector2 playerPos;

	// Token: 0x040000FB RID: 251
	[HideInInspector]
	public int startAmmo;

	// Token: 0x040000FC RID: 252
	public float moveForce = 365f;

	// Token: 0x040000FD RID: 253
	private float maxSpeed = 8f;

	//public float jumpForce = 4200f;
}
