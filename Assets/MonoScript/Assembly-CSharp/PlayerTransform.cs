﻿using System;
using System.Collections;
using UnityEngine;

// Token: 0x02000024 RID: 36
public class PlayerTransform : MonoBehaviour
{
	// Token: 0x060000E8 RID: 232 RVA: 0x00009920 File Offset: 0x00007B20
	private void Awake()
	{
	}

	// Token: 0x060000E9 RID: 233 RVA: 0x00009924 File Offset: 0x00007B24
	private void Start()
	{
		this.plCtrl = base.transform.root.GetComponent<PlayerControls>();
		this.plGames = base.transform.root.GetComponent<PlayerGames>();
		this.plLife = base.transform.root.GetComponent<PlayerLife>();
		base.StartCoroutine(this.colorPlayer());
	}

	// Token: 0x060000EA RID: 234 RVA: 0x00009980 File Offset: 0x00007B80
	private IEnumerator colorPlayer()
	{
		yield return new WaitForSeconds(0.2f);
		this.CoatSprites = base.gameObject.GetComponentsInChildren<SpriteRenderer>();
		foreach (SpriteRenderer oneSprite in this.CoatSprites)
		{
			if (oneSprite.tag == "Coat")
			{
				oneSprite.material = this.plMaterial;
			}
		}
		yield break;
	}

	// Token: 0x060000EB RID: 235 RVA: 0x0000999C File Offset: 0x00007B9C
	private void Update()
	{
	}

	// Token: 0x060000EC RID: 236 RVA: 0x000099A0 File Offset: 0x00007BA0
	public void setColor(Color myColor, Material myMaterial)
	{
		this.plMaterial = myMaterial;
		this.playerColor = myColor;
		base.StartCoroutine(this.colorPlayer());
	}

	// Token: 0x060000ED RID: 237 RVA: 0x000099C0 File Offset: 0x00007BC0
	public void colorCoat()
	{
		foreach (SpriteRenderer spriteRenderer in this.CoatSprites)
		{
			if (spriteRenderer.tag == "Coat")
			{
				spriteRenderer.material = this.plMaterial;
			}
		}
	}

	// Token: 0x060000EE RID: 238 RVA: 0x00009A10 File Offset: 0x00007C10
	public IEnumerator StartResetColor()
	{
		yield return new WaitForSeconds(0.1f);
		this.resetColor();
		yield break;
	}

	// Token: 0x060000EF RID: 239 RVA: 0x00009A2C File Offset: 0x00007C2C
	public void hurtColor()
	{
		SpriteRenderer[] componentsInChildren = base.gameObject.GetComponentsInChildren<SpriteRenderer>();
		foreach (SpriteRenderer spriteRenderer in componentsInChildren)
		{
		}
		this.showHealthbar();
	}

	// Token: 0x060000F0 RID: 240 RVA: 0x00009A68 File Offset: 0x00007C68
	public void hideColor()
	{
		SpriteRenderer[] componentsInChildren = base.gameObject.GetComponentsInChildren<SpriteRenderer>();
		foreach (SpriteRenderer spriteRenderer in componentsInChildren)
		{
		}
		this.showHealthbar();
	}

	// Token: 0x060000F1 RID: 241 RVA: 0x00009AA4 File Offset: 0x00007CA4
	public void resetColor()
	{
		SpriteRenderer[] componentsInChildren = base.gameObject.GetComponentsInChildren<SpriteRenderer>();
		foreach (SpriteRenderer spriteRenderer in componentsInChildren)
		{
		}
		this.showHealthbar();
		this.colorCoat();
	}

	// Token: 0x060000F2 RID: 242 RVA: 0x00009AE4 File Offset: 0x00007CE4
	public void makeInvisible(bool hide = true)
	{
		this.isHiding = hide;
		SpriteRenderer[] componentsInChildren = base.gameObject.GetComponentsInChildren<SpriteRenderer>();
		foreach (SpriteRenderer spriteRenderer in componentsInChildren)
		{
			spriteRenderer.enabled = !hide;
		}
		base.GetComponent<Rigidbody2D>().velocity = new Vector2(0f, 0f);
		this.plCtrl.canShoot = !hide;
		this.plCtrl.canMove = !hide;
		this.plLife.invincible = hide;
		if (hide)
		{
			base.gameObject.layer = 11;
		}
		else
		{
			this.plGames.changeDisks();
			base.gameObject.layer = 10;
		}
		base.transform.root.Find("Shadow").GetComponent<Projector>().enabled = !hide;
	}

	// Token: 0x060000F3 RID: 243 RVA: 0x00009BC0 File Offset: 0x00007DC0
	public IEnumerator flickerPlayer()
	{
		int i = 0;
		while (i < 12)
		{
			float alphaValue = 1f;
			if (i % 2 == 0)
			{
				alphaValue = 0.5f;
			}
			SpriteRenderer[] allSprites = base.gameObject.GetComponentsInChildren<SpriteRenderer>();
			foreach (SpriteRenderer oneSprite in allSprites)
			{
				Color actColor = oneSprite.color;
				actColor.a = alphaValue;
				oneSprite.color = actColor;
			}
			i++;
			yield return new WaitForSeconds(0.15f);
		}
		yield break;
	}

	// Token: 0x060000F4 RID: 244 RVA: 0x00009BDC File Offset: 0x00007DDC
	public void showHealthbar()
	{
		SpriteRenderer component = base.transform.root.Find("_healthbarBG").GetComponent<SpriteRenderer>();
		if (component != null)
		{
			component.color = new Color(0f, 0f, 0f, 1f);
		}
	}

	// Token: 0x04000120 RID: 288
	public Color playerColor;

	// Token: 0x04000121 RID: 289
	public Material plMaterial;

	// Token: 0x04000122 RID: 290
	private SpriteRenderer[] CoatSprites;

	// Token: 0x04000123 RID: 291
	private PlayerControls plCtrl;

	// Token: 0x04000124 RID: 292
	private PlayerLife plLife;

	// Token: 0x04000125 RID: 293
	private PlayerGames plGames;

	// Token: 0x04000126 RID: 294
	public bool isHiding;
}
