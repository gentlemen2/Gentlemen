﻿using System;
using System.Collections;
using UnityEngine;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;

// Token: 0x02000018 RID: 24
public class Main : MonoBehaviour
{
	// Token: 0x06000089 RID: 137 RVA: 0x00004DD8 File Offset: 0x00002FD8
	private void Awake()
	{
		this.msgTex = GameObject.Find("GuiMsg").GetComponent<GUITexture>();
		this.Scores = new int[5];
		this.Downloads = new int[5];
		this.Disks = new int[5];
		this.BombKills = new int[5];
		this.allPlayers = new GameObject[5];
		if (GameObject.Find("_Globals") != null)
		{
			this.testMode = false;
			this.GlobalStats = GameObject.Find("_Globals").GetComponent<Globals>();
			this.anzPlayer = this.GlobalStats.g_anzPlayer;
			this.gameTime = this.GlobalStats.g_gameTime * 60;
			this.gameMode = this.GlobalStats.g_gameMode;
			this.gameGoals = this.GlobalStats.g_gameGoals;
			this.plColors = this.GlobalStats.g_plColors;
			this.oneShotKiller = this.GlobalStats.g_modeOneShot;
			this.modeMelee = this.GlobalStats.g_modeMelee;
			this.modeThrowLT = this.GlobalStats.g_modeThrowLT;
		}
		if (this.anzPlayer < 4)
		{
			Object.Destroy(GameObject.Find("Player4"));
			Object.Destroy(GameObject.Find("Terminal4"));
		}
		if (this.anzPlayer < 3)
		{
			Object.Destroy(GameObject.Find("Player3"));
			Object.Destroy(GameObject.Find("Terminal3"));
		}
	}

	// Token: 0x0600008A RID: 138 RVA: 0x00004F48 File Offset: 0x00003148
	private void Start()
	{
		for (int i = 0; i <= this.anzPlayer; i++)
		{
			this.allPlayers[i] = GameObject.Find("Player" + i);
		}
		this.prepareGame();
		base.StartCoroutine("startSplash");
		this.enablePlayers(false);
		for (int j = 1; j <= this.anzPlayer; j++)
		{
			this.allPlayers[j].GetComponent<PlayerTransform>().setColor(this.plColors[j], this.plMaterials[j]);
			GameObject.Find("Terminal" + j).GetComponent<Terminal>().setTermColor(this.plColors[j]);
		}
	}

	// Token: 0x0600008B RID: 139 RVA: 0x00005014 File Offset: 0x00003214
	private void prepareGame()
	{
		if (this.gameMode != 1)
		{
			Object.Destroy(GameObject.Find("Laptop"));
		}
		if (this.gameMode != 2)
		{
			Object.Destroy(GameObject.Find("DiskMaster"));
			for (int i = 1; i <= 4; i++)
			{
				Object.Destroy(GameObject.Find("Terminal" + i));
			}
		}
		if (this.gameMode != 3)
		{
			Object.Destroy(GameObject.Find("Bomb"));
		}
	}

	// Token: 0x0600008C RID: 140 RVA: 0x000050A0 File Offset: 0x000032A0
	private void checkGoals()
	{
		if (this.gameMode == 0 && this.gameGoals[0] > 0)
		{
			for (int i = 1; i <= this.anzPlayer; i++)
			{
				if (this.Kills[i] == this.gameGoals[0] && this.winner == 0)
				{
					MonoBehaviour.print("Player " + i + " won after Kills!");
					this.winner = i;
					base.StartCoroutine("gameoverSplash");
					break;
				}
			}
		}
		if (this.gameMode == 1 && !this.gameOver)
		{
			for (int j = 1; j <= this.anzPlayer; j++)
			{
				if (this.Downloads[j] >= this.gameGoals[1])
				{
					MonoBehaviour.print(string.Concat(new object[]
					{
						"Player ",
						j,
						" hat ",
						this.Downloads[j],
						" von ",
						this.gameGoals[1]
					}));
					this.winner = j;
					base.StartCoroutine("gameoverSplash");
				}
			}
		}
		if (this.gameMode == 2)
		{
			for (int k = 1; k <= this.anzPlayer; k++)
			{
				if (this.Disks[k] >= this.gameGoals[2] && this.winner == 0)
				{
					MonoBehaviour.print("Player " + k + " won after Disks!");
					this.winner = k;
					base.StartCoroutine("gameoverSplash");
					break;
				}
			}
		}
		if (this.gameMode == 3)
		{
			for (int l = 1; l <= this.anzPlayer; l++)
			{
				if (this.BombKills[l] <= this.gameGoals[3] && this.winner == 0)
				{
					MonoBehaviour.print("Player " + l + " lost after Disks!");
					this.winner = l;
					base.StartCoroutine("gameoverSplash");
					break;
				}
			}
		}
	}

	// Token: 0x0600008D RID: 141 RVA: 0x000052C0 File Offset: 0x000034C0
	private void checkGoalsTime()
	{
		if (this.winner == 0 && this.gameMode == 0)
		{
			int num = -999;
			int num2 = 0;
			for (int i = 1; i <= this.anzPlayer; i++)
			{
				if (this.Kills[i] > num)
				{
					num = this.Kills[i];
					this.winner = i;
				}
			}
			for (int j = 1; j <= this.anzPlayer; j++)
			{
				if (this.Kills[j] == num)
				{
					num2++;
				}
			}
			if (num2 > 1)
			{
				this.drawGame = true;
			}
			base.StartCoroutine("gameoverSplash");
		}
	}

	// Token: 0x0600008E RID: 142 RVA: 0x00005364 File Offset: 0x00003564
	private void Update()
	{
		if ((Input.GetKeyDown(KeyCode.Escape) || Input.GetKeyDown(KeyCode.P)) && !this.gameOver && !this.starting)
		{
			this.stopGame(true);
		}
		if (this.gameOver)
		{
			this.enablePlayers(false);
		}
	}

	// Token: 0x0600008F RID: 143 RVA: 0x000053B8 File Offset: 0x000035B8
	public void setKills(int killerID, bool isSuicide)
	{
		int num = 1;
		if (isSuicide)
		{
			num *= -1;
		}
		this.Kills[killerID] += num;
		if (!this.gameOver && this.gameMode == 0)
		{
			GameObject.Find("GUI").GetComponent<GuiScore>().setScore(this.Kills, this.anzPlayer);
		}
		this.checkGoals();
	}

	// Token: 0x06000090 RID: 144 RVA: 0x00005420 File Offset: 0x00003620
	public void setDownload(int playerID)
	{
		if (!this.gameOver && this.gameMode == 1)
		{
			this.Downloads[playerID]++;
			GameObject.Find("GUI").GetComponent<GuiScore>().setScore(this.Downloads, this.anzPlayer);
		}
		if (this.Downloads[playerID] >= this.gameGoals[1])
		{
			this.checkGoals();
		}
	}

	// Token: 0x06000091 RID: 145 RVA: 0x00005490 File Offset: 0x00003690
	public void setDisks(int playerID, int nrDisks)
	{
		this.Disks[playerID] += nrDisks;
		if (!this.gameOver && this.gameMode == 2)
		{
			GameObject.Find("GUI").GetComponent<GuiScore>().setScore(this.Disks, this.anzPlayer);
		}
		this.checkGoals();
	}

	// Token: 0x06000092 RID: 146 RVA: 0x000054EC File Offset: 0x000036EC
	public void setBombkill(int playerID)
	{
		int num = 1;
		this.BombKills[playerID] -= num;
		if (!this.gameOver && this.gameMode == 3)
		{
			GameObject.Find("GUI").GetComponent<GuiScore>().setScore(this.BombKills, this.anzPlayer);
		}
		this.checkGoals();
	}

	// Token: 0x06000093 RID: 147 RVA: 0x0000554C File Offset: 0x0000374C
	public IEnumerator runTimer()
	{
		yield return new WaitForSeconds(1f);
		if (this.gameTime > 0 && !this.gameOver)
		{
			this.gameTime--;
			base.StartCoroutine("runTimer");
		}
		else
		{
			this.checkGoalsTime();
		}
		yield break;
	}

	// Token: 0x06000094 RID: 148 RVA: 0x00005568 File Offset: 0x00003768
	public IEnumerator startSplash()
	{
		float splashTime = 3f;
		if (this.testMode)
		{
			splashTime = 0f;
		}
		this.starting = true;
		yield return new WaitForSeconds(splashTime);
		this.starting = false;
		this.msgTex.enabled = false;
		Time.timeScale = 1f;
		if (this.gameTime > 0)
		{
			base.StartCoroutine("runTimer");
		}
		base.StartCoroutine("hidePlayerNames");
		this.enablePlayers(true);
		yield break;
	}

	// Token: 0x06000095 RID: 149 RVA: 0x00005584 File Offset: 0x00003784
	private IEnumerator hidePlayerNames()
	{
		yield return new WaitForSeconds(3f);
		this.showPlayerNames = false;
		yield break;
	}

	// Token: 0x06000096 RID: 150 RVA: 0x000055A0 File Offset: 0x000037A0
	public IEnumerator gameoverSplash()
	{
		this.msgTex.enabled = true;
		base.GetComponent<AudioSource>().Play();
		this.gameOver = true;
		this.enablePlayers(false);
		yield return new WaitForSeconds(300f);
		Application.LoadLevel("MainMenu");
		yield break;
	}

	// Token: 0x06000097 RID: 151 RVA: 0x000055BC File Offset: 0x000037BC
	public void stopGame(bool makePause = true)
	{
		this.paused = !this.paused;
		if (!makePause)
		{
			this.paused = false;
		}
		this.msgTex.enabled = this.paused;
		if (this.paused)
		{
			Time.timeScale = 0f;
		}
		else
		{
			Time.timeScale = 1f;
		}
	}

	// Token: 0x06000098 RID: 152 RVA: 0x0000561C File Offset: 0x0000381C
	private void OnGUI()
	{
		GUI.skin = this.SmartSkin;
		Color color = new Color(0.35f, 0.35f, 0.4f, 1f);
		Color color2 = new Color(0.92f, 0.89f, 0.74f, 1f);
		GUIStyle guistyle = new GUIStyle();
		guistyle.normal.textColor = color2;
		guistyle.alignment = TextAnchor.MiddleLeft;
		guistyle.fontSize = 65;
		guistyle.font = this.guiFont;
		if (this.starting)
		{
			this.msgTex.enabled = true;
			string text = string.Empty;
			switch (this.gameMode)
			{
			case 0:
				text = "GET READY!";
				break;
			case 1:
				text = "GET " + this.gameGoals[1] + " DOWNLOADS\nFROM LAPTOP!";
				break;
			case 2:
				text = "GET " + this.gameGoals[2] + " DISCS TO\nYOUR TERMINAL!";
				break;
			case 3:
				text = "GET RID OF THAT\nTIME BOMB!";
				break;
			}
			GUI.Label(new Rect((float)Screen.width * 0.38f, (float)Screen.height * 0.48f, (float)Screen.width * 0.2f, (float)Screen.height * 0.05f), text, guistyle);
		}
		if (this.showPlayerNames)
		{
			Camera component = GameObject.Find("_MainCam").GetComponent<Camera>();
			float orthographicSize = component.orthographicSize;
			Vector2 vector = new Vector2(orthographicSize / -20f, orthographicSize * 1.2f);
			for (int i = 1; i <= this.anzPlayer; i++)
			{
				Vector3 position = this.allPlayers[i].transform.position;
				Vector3 vector2 = new Vector3(position.x + vector.x, position.y + vector.y, position.z);
				Vector3 vector3 = component.WorldToScreenPoint(vector2);
				string text2 = "Player " + i;
				guistyle.fontSize = 20;
				GUI.Label(new Rect(vector3.x, (float)Screen.height * 1.5f - vector3.y, 50f, 20f), text2, guistyle);
			}
		}
		if (this.gameOver)
		{
			GUI.Label(new Rect((float)Screen.width * 0.35f, (float)Screen.height * 0.4f, (float)Screen.width * 0.2f, (float)Screen.height * 0.05f), "GAME OVER", guistyle);
			string text3 = string.Empty;
			if (this.drawGame)
			{
				text3 = "Win Win Situation.";
			}
			else
			{
				if (this.gameMode == 3)
				{
					text3 = "Player " + this.winner + " lost!";
					this.allPlayers[this.winner].GetComponent<PlayerLife>().killPlayer(0);
				}
				else
				{
					text3 = "Player " + this.winner + " won!";
					for (int j = 1; j <= this.anzPlayer; j++)
					{
						this.allPlayers[j].GetComponent<PlayerControls>().animArms.SetFloat("Speed", 0f);
						this.allPlayers[j].GetComponent<PlayerControls>().anim.SetFloat("Speed", 0f);
						if (this.allPlayers[j].GetComponent<PlayerControls>().playerID != this.winner)
						{
							this.allPlayers[j].GetComponent<PlayerLife>().killPlayer(0);
						}
					}
				}
				this.msgTex.color = this.plColors[this.winner];
			}
			GUI.Label(new Rect((float)Screen.width * 0.4f, (float)Screen.height * 0.52f, (float)Screen.width * 0.2f, (float)Screen.height * 0.05f), text3, guistyle);
			if (GUI.Button(new Rect((float)Screen.width * 0.4f, (float)Screen.height * 0.6f, (float)Screen.width * 0.2f, (float)Screen.height * 0.05f), "Replay This Game"))
			{
				this.stopGame(false);
				GUI.enabled = false;
				Application.LoadLevel(this.GlobalStats.g_level);
			}
			if (GUI.Button(new Rect((float)Screen.width * 0.4f, (float)Screen.height * 0.68f, (float)Screen.width * 0.2f, (float)Screen.height * 0.05f), "Main Menu"))
			{
				this.stopGame(false);
				Application.LoadLevel("MainMenu");
			}
		}
		if (this.paused)
		{
			GUI.Label(new Rect((float)Screen.width * 0.4f, (float)Screen.height * 0.35f, (float)Screen.width * 0.2f, (float)Screen.height * 0.05f), "PAUSE", guistyle);
			if (GUI.Button(new Rect((float)Screen.width * 0.4f, (float)Screen.height * 0.45f, (float)Screen.width * 0.2f, (float)Screen.height * 0.05f), "Quit Game"))
			{
				this.stopGame(false);
				Application.LoadLevel("MainMenu");
			}
			if (GUI.Button(new Rect((float)Screen.width * 0.4f, (float)Screen.height * 0.55f, (float)Screen.width * 0.2f, (float)Screen.height * 0.05f), "Resume Game"))
			{
				this.stopGame(false);
				GUI.enabled = false;
			}
		}
	}

	// Token: 0x06000099 RID: 153 RVA: 0x00005BC0 File Offset: 0x00003DC0
	private void enablePlayers(bool toggle)
	{
		GameObject[] array = GameObject.FindGameObjectsWithTag("Player");
		foreach (GameObject gameObject in array)
		{
			gameObject.GetComponent<PlayerControls>().canMove = toggle;
			gameObject.GetComponent<PlayerControls>().canShoot = toggle;
			gameObject.GetComponent<Animator>().SetFloat("Speed", 0f);
		}
	}

	// Token: 0x04000088 RID: 136
	public Globals GlobalStats;

	// Token: 0x04000089 RID: 137
	public int anzPlayer;

	// Token: 0x0400008A RID: 138
	public int gameTime;

	// Token: 0x0400008B RID: 139
	public int gameMode;

	// Token: 0x0400008C RID: 140
	public bool oneShotKiller;

	// Token: 0x0400008D RID: 141
	public bool modeMelee = true;

	// Token: 0x0400008E RID: 142
	public bool modeThrowLT = true;

	// Token: 0x0400008F RID: 143
	public int[] gameGoals;

	// Token: 0x04000090 RID: 144
	public int[] Scores;

	// Token: 0x04000091 RID: 145
	public int[] Kills;

	// Token: 0x04000092 RID: 146
	public int[] Downloads;

	// Token: 0x04000093 RID: 147
	public int[] Disks;

	// Token: 0x04000094 RID: 148
	public int[] BombKills;

	// Token: 0x04000095 RID: 149
	[HideInInspector]
	public int winner;

	// Token: 0x04000096 RID: 150
	[HideInInspector]
	public bool drawGame;

	// Token: 0x04000097 RID: 151
	[HideInInspector]
	public float FloorHeight = 5f;

	// Token: 0x04000098 RID: 152
	public GameObject[] allPlayers;

	// Token: 0x04000099 RID: 153
	public Color[] plColors;

	// Token: 0x0400009A RID: 154
	public Material[] plMaterials;

	// Token: 0x0400009B RID: 155
	public bool paused;

	// Token: 0x0400009C RID: 156
	private bool starting;

	// Token: 0x0400009D RID: 157
	public bool gameOver;

	// Token: 0x0400009E RID: 158
	private bool testMode = true;

	// Token: 0x0400009F RID: 159
	public GUISkin SmartSkin;

	// Token: 0x040000A0 RID: 160
	private GUITexture msgTex;

	// Token: 0x040000A1 RID: 161
	public Font guiFont;

	// Token: 0x040000A2 RID: 162
	private bool showPlayerNames = true;
}
