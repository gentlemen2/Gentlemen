﻿using System;
using System.Collections;
using UnityEngine;

// Token: 0x0200002E RID: 46
public class Water : MonoBehaviour
{
	// Token: 0x0600012C RID: 300 RVA: 0x0000B340 File Offset: 0x00009540
	private void Start()
	{
		this.startYPos = base.transform.position.y;
		this.nextYGoal = base.transform.position.y + this.maxYDifference;
		this.moveTide();
	}

	// Token: 0x0600012D RID: 301 RVA: 0x0000B38C File Offset: 0x0000958C
	private void Update()
	{
		if (((base.transform.position.y < this.startYPos && this.direction == 1) || (base.transform.position.y > this.nextYGoal && this.direction == -1)) && this.isBusy)
		{
			base.GetComponent<Rigidbody2D>().velocity = new Vector2(0f, 0f);
			this.isBusy = false;
			base.StartCoroutine(this.changeDir());
		}
	}

	// Token: 0x0600012E RID: 302 RVA: 0x0000B428 File Offset: 0x00009628
	public void moveTide()
	{
		if (base.GetComponent<Rigidbody2D>().velocity.y == 0f && !this.isBusy && this.direction != 0)
		{
			this.direction *= -1;
			MonoBehaviour.print("Bewegung auslösen: " + this.direction);
			base.GetComponent<Rigidbody2D>().velocity = new Vector2(0f, this.speed * (float)(-(float)this.direction));
			this.isBusy = true;
		}
	}

	// Token: 0x0600012F RID: 303 RVA: 0x0000B4BC File Offset: 0x000096BC
	public IEnumerator changeDir()
	{
		yield return new WaitForSeconds(2f);
		this.moveTide();
		yield break;
	}

	// Token: 0x06000130 RID: 304 RVA: 0x0000B4D8 File Offset: 0x000096D8
	private void OnTriggerStay2D(Collider2D col)
	{
		if (col.tag == "Player")
		{
			PlayerControls component = col.GetComponent<PlayerControls>();
			if (component.activate == 1)
			{
				col.GetComponent<Rigidbody2D>().AddForce(new Vector2(0f, 150f));
			}
		}
	}

	// Token: 0x06000131 RID: 305 RVA: 0x0000B528 File Offset: 0x00009728
	private void OnTriggerExit2D(Collider2D col)
	{
		PlayerControls component = col.GetComponent<PlayerControls>();
	}

	// Token: 0x0400016F RID: 367
	private float startYPos;

	// Token: 0x04000170 RID: 368
	private float maxYDifference = 7f;

	// Token: 0x04000171 RID: 369
	public int direction = 1;

	// Token: 0x04000172 RID: 370
	private float speed = 0.05f;

	// Token: 0x04000173 RID: 371
	private float nextYGoal;

	// Token: 0x04000174 RID: 372
	private bool isBusy;
}
