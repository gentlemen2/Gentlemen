﻿using System;
using System.Collections;
using UnityEngine;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;

// Token: 0x02000014 RID: 20
public class Timebomb : MonoBehaviour
{
	// Token: 0x06000071 RID: 113 RVA: 0x00004570 File Offset: 0x00002770
	private void Start()
	{
		base.transform.GetComponent<SpriteRenderer>().enabled = false;
		this.audioSources = new AudioSource[this.audioClips.Length];
		for (int i = 0; i < this.audioClips.Length; i++)
		{
			GameObject gameObject = new GameObject("AudioPlayer");
			gameObject.transform.parent = base.gameObject.transform;
			this.audioSources[i] = gameObject.AddComponent<AudioSource>() as AudioSource;
			this.audioSources[i].clip = this.audioClips[i];
		}
		this.mainScript = GameObject.Find("_MainScript").GetComponent<Main>();
		if (this.mainScript.gameMode == 3)
		{
			base.StartCoroutine("makeFirstBomb");
		}
		this.activeColor = base.transform.GetComponent<SpriteRenderer>().color;
	}

	// Token: 0x06000072 RID: 114 RVA: 0x00004650 File Offset: 0x00002850
	private void OnTriggerEnter2D(Collider2D col)
	{
		if (col.tag == "Player" && !col.transform.root.GetComponent<PlayerLife>().killed && this.canBeCollected)
		{
			this.audioSources[1].Play();
			base.GetComponent<SpriteRenderer>().color = this.activeColor;
			this.addToPlayer(col.transform);
			base.StartCoroutine("pauseCapturing");
		}
	}

	// Token: 0x06000073 RID: 115 RVA: 0x000046D0 File Offset: 0x000028D0
	private void addToPlayer(Transform newPlayer)
	{
		if (this.actPlGames != null)
		{
			this.actPlGames.hasBomb = false;
		}
		this.actPlayer = newPlayer;
		base.transform.parent = this.actPlayer;
		this.actPlGames = this.actPlayer.root.GetComponent<PlayerGames>();
		this.actPlGames.hasBomb = true;
		base.transform.position = new Vector2(this.actPlayer.position.x, this.actPlayer.position.y + 0.5f);
	}

	// Token: 0x06000074 RID: 116 RVA: 0x00004778 File Offset: 0x00002978
	public IEnumerator pauseCapturing()
	{
		this.canBeCollected = false;
		base.transform.GetComponent<SpriteRenderer>().color = this.inactiveColor;
		yield return new WaitForSeconds(1.5f);
		base.transform.GetComponent<SpriteRenderer>().color = this.activeColor;
		this.canBeCollected = true;
		yield break;
	}

	// Token: 0x06000075 RID: 117 RVA: 0x00004794 File Offset: 0x00002994
	private void initBomb()
	{
		this.canBeCollected = true;
		for (int i = 0; i < this.intervals.Length; i++)
		{
			this.intervals[i] = Random.Range(this.inv_durMin[i], this.inv_durMax[i]);
		}
		base.StartCoroutine("intervalRun");
		base.StartCoroutine("intervalChange");
		int num = Random.Range(1, this.mainScript.anzPlayer + 1);
		this.actPlayer = GameObject.Find("Player" + num).transform;
		this.addToPlayer(this.actPlayer);
		base.transform.GetComponent<SpriteRenderer>().enabled = true;
		this.Display.GetComponent<SpriteRenderer>().enabled = true;
		base.StartCoroutine("setTimer");
		this.audioSources[0].Play();
	}

	// Token: 0x06000076 RID: 118 RVA: 0x00004874 File Offset: 0x00002A74
	public void explode()
	{
		base.StopCoroutine("setTimer");
		base.StopCoroutine("waitForRespawn");
		base.StopCoroutine("intervalRun");
		base.StopCoroutine("intervalChange");
		this.actInterval = 0;
		this.audioSources[2].Stop();
		this.audioSources[2].loop = false;
		base.transform.parent = null;
		this.canBeCollected = false;
		base.transform.GetComponent<SpriteRenderer>().enabled = false;
		this.Display.GetComponent<SpriteRenderer>().enabled = false;
		float num = (float)Random.Range(-1, 1);
		if (num == 0f)
		{
			num = 1f;
		}
		this.mainScript.GetComponent<MainFX>().explode(new Vector2(base.transform.position.x + 0.1f * num, base.transform.position.y));
		this.actPlayer.GetComponent<PlayerLife>().killPlayer(0);
		int playerID = this.actPlayer.GetComponent<PlayerControls>().playerID;
		this.mainScript.setBombkill(playerID);
		base.StartCoroutine("waitForRespawn");
	}

	// Token: 0x06000077 RID: 119 RVA: 0x0000499C File Offset: 0x00002B9C
	private IEnumerator intervalRun()
	{
		if (this.actInterval < this.intervals.Length - 1)
		{
			yield return new WaitForSeconds(this.inv_bpm[this.actInterval] * 0.3f);
			this.audioSources[2].Play();
			this.Display.GetComponent<SpriteRenderer>().color = Color.red;
			yield return new WaitForSeconds(0.3f);
			this.Display.GetComponent<SpriteRenderer>().color = Color.black;
			base.StartCoroutine("intervalRun");
		}
		else
		{
			this.audioSources[2].loop = true;
			this.audioSources[2].Play();
			this.Display.GetComponent<SpriteRenderer>().color = Color.red;
		}
		yield break;
	}

	// Token: 0x06000078 RID: 120 RVA: 0x000049B8 File Offset: 0x00002BB8
	private IEnumerator intervalChange()
	{
		if (this.actInterval < this.intervals.Length)
		{
			yield return new WaitForSeconds(this.intervals[this.actInterval]);
			this.actInterval++;
			base.StartCoroutine("intervalChange");
		}
		else
		{
			this.explode();
		}
		yield break;
	}

	// Token: 0x06000079 RID: 121 RVA: 0x000049D4 File Offset: 0x00002BD4
	private IEnumerator setTimer()
	{
		yield return new WaitForSeconds(70f);
		this.explode();
		yield break;
	}

	// Token: 0x0600007A RID: 122 RVA: 0x000049F0 File Offset: 0x00002BF0
	private IEnumerator waitForRespawn()
	{
		int respawnTime = Random.Range(2, 8);
		yield return new WaitForSeconds((float)respawnTime);
		this.initBomb();
		yield break;
	}

	// Token: 0x0600007B RID: 123 RVA: 0x00004A0C File Offset: 0x00002C0C
	private IEnumerator makeFirstBomb()
	{
		yield return new WaitForSeconds(3f);
		this.initBomb();
		yield break;
	}

	// Token: 0x04000066 RID: 102
	public bool canBeCollected = true;

	// Token: 0x04000067 RID: 103
	public Transform Display;

	// Token: 0x04000068 RID: 104
	private float[] intervals = new float[5];

	// Token: 0x04000069 RID: 105
	private float[] inv_bpm = new float[] { 8f, 5f, 3f, 1f, 1f };

	// Token: 0x0400006A RID: 106
	private float[] inv_durMin = new float[] { 4f, 3f, 2.5f, 1f, 0.5f };

	// Token: 0x0400006B RID: 107
	private float[] inv_durMax = new float[] { 12f, 10f, 8f, 6f, 3.5f };

	// Token: 0x0400006C RID: 108
	private int actInterval;

	// Token: 0x0400006D RID: 109
	private Main mainScript;

	// Token: 0x0400006E RID: 110
	private Transform actPlayer;

	// Token: 0x0400006F RID: 111
	private PlayerGames actPlGames;

	// Token: 0x04000070 RID: 112
	private Color activeColor;

	// Token: 0x04000071 RID: 113
	private Color inactiveColor = new Color(0.5f, 0.5f, 0.5f, 1f);

	// Token: 0x04000072 RID: 114
	public AudioClip[] audioClips;

	// Token: 0x04000073 RID: 115
	private AudioSource[] audioSources;
}
