﻿using System;
using UnityEngine;

// Token: 0x02000006 RID: 6
public class autoLift : MonoBehaviour
{
	// Token: 0x0600001A RID: 26 RVA: 0x00002784 File Offset: 0x00000984
	private void Start()
	{
		if (this.goesUpFromHere)
		{
			this.yStart = base.transform.position.y;
		}
		else
		{
			this.yStart = base.transform.position.y - this.yDistance;
		}
		MonoBehaviour.print(this.yStart);
	}

	// Token: 0x0600001B RID: 27 RVA: 0x000027EC File Offset: 0x000009EC
	private void Update()
	{
		if ((double)base.transform.position.y <= (double)this.yStart + 0.1 && base.transform.position.y < this.yStart + this.yDistance)
		{
			this.goUp = true;
		}
		if (base.transform.position.y > this.yStart + this.yDistance)
		{
			this.goUp = false;
		}
		if (this.goUp)
		{
			base.GetComponent<Rigidbody2D>().velocity = new Vector2(0f, this.speed);
		}
		else
		{
			base.GetComponent<Rigidbody2D>().velocity = new Vector2(0f, -this.speed);
		}
	}

	// Token: 0x04000012 RID: 18
	public float speed = 1f;

	// Token: 0x04000013 RID: 19
	[HideInInspector]
	public float yStart;

	// Token: 0x04000014 RID: 20
	public float yDistance = 2f;

	// Token: 0x04000015 RID: 21
	[HideInInspector]
	public bool goUp = true;

	// Token: 0x04000016 RID: 22
	public bool goesUpFromHere = true;
}
