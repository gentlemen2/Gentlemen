﻿using System;
using System.Collections;
using UnityEngine;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;

// Token: 0x02000019 RID: 25
public class MainFX : MonoBehaviour
{
	// Token: 0x0600009B RID: 155 RVA: 0x00005C34 File Offset: 0x00003E34
	private void Start()
	{
		this.mainCam = GameObject.Find("_MainCam").GetComponent<Camera>();
		this.firstCamPos = this.mainCam.transform.position;
	}

	// Token: 0x0600009C RID: 156 RVA: 0x00005C6C File Offset: 0x00003E6C
	private void Update()
	{
	}

	// Token: 0x0600009D RID: 157 RVA: 0x00005C70 File Offset: 0x00003E70
	public void explode(Vector2 explosionPos)
	{
		base.StartCoroutine("shakeCam");
		ParticleSystem particleSystem = Object.Instantiate(this.partExplode, explosionPos, Quaternion.Euler(new Vector3(90f, 0f, 0f))) as ParticleSystem;
		Vector2 vector = new Vector2(explosionPos.x - 10f, explosionPos.y + 2f);
		Vector2 vector2 = new Vector2(explosionPos.x + 10f, explosionPos.y - 2f);
		Collider2D[] array = Physics2D.OverlapAreaAll(vector, vector2);
		foreach (Collider2D collider2D in array)
		{
			if (collider2D)
			{
				float num = collider2D.transform.position.x - explosionPos.x;
				int num2 = 1;
				if (num <= 0f)
				{
					num2 = -1;
				}
				if (num > -0.3f && num < 0.3f)
				{
					num *= 5f;
				}
				if (num == 0f || float.IsInfinity(num))
				{
					num2 = Random.Range(-1, 2);
					if (num == 0f)
					{
					}
					num = 1f * (float)num2;
				}
				float num3 = 100f / num * (float)num2;
				if (collider2D.tag == "Player")
				{
					collider2D.GetComponent<PlayerLife>().Hurt(0, num3);
				}
				else
				{
					collider2D.SendMessage("damageObject", num3, SendMessageOptions.DontRequireReceiver);
				}
				if (collider2D.GetComponent<Rigidbody2D>() && !base.GetComponent<Rigidbody2D>())
				{
					collider2D.GetComponent<Rigidbody2D>().AddForce(new Vector2(500f / num, 50f));
				}
			}
		}
	}

	// Token: 0x0600009E RID: 158 RVA: 0x00005E50 File Offset: 0x00004050
	public IEnumerator shakeCam()
	{
		for (int i = 0; i <= 10; i++)
		{
			Vector3 newPos = new Vector3((float)Random.Range(-1, 3) * this.shakeForce, (float)Random.Range(-1, 3) * this.shakeForce, this.firstCamPos.z);
			this.mainCam.transform.position = newPos;
			yield return new WaitForSeconds(0.01f);
		}
		yield return new WaitForSeconds(0.1f);
		this.mainCam.transform.position = this.firstCamPos;
		yield break;
	}

	// Token: 0x040000A3 RID: 163
	private Camera mainCam;

	// Token: 0x040000A4 RID: 164
	private Vector3 firstCamPos;

	// Token: 0x040000A5 RID: 165
	public ParticleSystem partExplode;

	// Token: 0x040000A6 RID: 166
	private float shakeForce = 0.5f;
}
