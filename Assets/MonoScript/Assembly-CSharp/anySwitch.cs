﻿using System;
using System.Collections;
using UnityEngine;

// Token: 0x0200002F RID: 47
public class anySwitch : MonoBehaviour
{
	// Token: 0x06000133 RID: 307 RVA: 0x0000B558 File Offset: 0x00009758
	private void Start()
	{
	}

	// Token: 0x06000134 RID: 308 RVA: 0x0000B55C File Offset: 0x0000975C
	private void Update()
	{
	}

	// Token: 0x06000135 RID: 309 RVA: 0x0000B560 File Offset: 0x00009760
	private void OnTriggerStay2D(Collider2D col)
	{
		if (col.tag == "Player")
		{
			int activate = col.GetComponent<PlayerControls>().activate;
			if (!this.isBusy)
			{
				if (!this.activByKeyUP)
				{
					this.activateSwitch();
				}
				if (this.activByKeyUP && activate == 1)
				{
					base.GetComponent<AudioSource>().Play();
					this.activateSwitch();
				}
			}
		}
	}

	// Token: 0x06000136 RID: 310 RVA: 0x0000B5D0 File Offset: 0x000097D0
	public void activateSwitch()
	{
		this.isBusy = true;
		foreach (GameObject gameObject in this.receivers)
		{
			gameObject.SendMessage("switchOn", this.optionalFloat, SendMessageOptions.DontRequireReceiver);
		}
		base.StartCoroutine("pauseTouch");
	}

	// Token: 0x06000137 RID: 311 RVA: 0x0000B628 File Offset: 0x00009828
	public IEnumerator pauseTouch()
	{
		yield return new WaitForSeconds(this.idleTime);
		this.isBusy = false;
		yield break;
	}

	// Token: 0x04000175 RID: 373
	public GameObject[] receivers;

	// Token: 0x04000176 RID: 374
	public float idleTime = 0.5f;

	// Token: 0x04000177 RID: 375
	public bool activByKeyUP = true;

	// Token: 0x04000178 RID: 376
	public float optionalFloat;

	// Token: 0x04000179 RID: 377
	private bool isBusy;
}
