﻿using System;
using System.Collections;
using UnityEngine;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;

// Token: 0x02000025 RID: 37
public class Projectile : MonoBehaviour
{
	// Token: 0x060000F6 RID: 246 RVA: 0x00009C5C File Offset: 0x00007E5C
	private void Start()
	{
		Object.Destroy(base.gameObject, this.lifetime);
		base.StartCoroutine("slowDown");
	}

	// Token: 0x060000F7 RID: 247 RVA: 0x00009C7C File Offset: 0x00007E7C
	private void Update()
	{
	}

	// Token: 0x060000F8 RID: 248 RVA: 0x00009C80 File Offset: 0x00007E80
	private void OnTriggerEnter2D(Collider2D col)
	{
		if (col.GetComponent<Rigidbody2D>())
		{
			col.GetComponent<Rigidbody2D>().AddForce(base.GetComponent<Rigidbody2D>().velocity * this.force);
		}
		if (col.tag == "Player" && !col.gameObject.GetComponent<PlayerLife>().killed)
		{
			col.gameObject.GetComponent<PlayerLife>().Hurt(this.PlayerID, this.damage);
		}
		if (col.tag != "Nonshootable" && col.name != "projectile")
		{
			col.SendMessage("damageObject", this.damage, SendMessageOptions.DontRequireReceiver);
			Object.Destroy(base.gameObject);
		}
	}

	// Token: 0x060000F9 RID: 249 RVA: 0x00009D50 File Offset: 0x00007F50
	public IEnumerator slowDown()
	{
		for (float f = this.lifetime; f >= 0f; f -= 0.1f)
		{
			yield return new WaitForSeconds(0.7f);
			this.damage -= f;
		}
		yield break;
	}

	// Token: 0x04000127 RID: 295
	public int PlayerID;

	// Token: 0x04000128 RID: 296
	public float damage = 22f;

	// Token: 0x04000129 RID: 297
	private float force = 5f;

	// Token: 0x0400012A RID: 298
	public float lifetime = 2f;
}
