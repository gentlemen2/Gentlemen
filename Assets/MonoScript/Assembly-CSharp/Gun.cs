﻿using System;
using System.Collections;
using UnityEngine;
using Object = UnityEngine.Object;


// Token: 0x0200001D RID: 29
public class Gun : MonoBehaviour
{
	// Token: 0x060000B7 RID: 183 RVA: 0x000080A4 File Offset: 0x000062A4
	private void Start()
	{
		this.playerCtrl = base.transform.root.GetComponent<PlayerControls>();
		this.playerID = this.playerCtrl.playerID;
		this.getFireKey = this.playerCtrl.fireKey;
		this.guiAmmo = GameObject.Find("GuiAmmo").GetComponent<GuiAmmo>();
		base.StartCoroutine(this.loadFireKey());
		Main component = GameObject.Find("_MainScript").GetComponent<Main>();
		this.modeMelee = component.modeMelee;
		this.anim.SetBool("armed", true);
		this.anim.SetBool("alive", true);
		this.audioSources = new AudioSource[this.audioClips.Length];
		for (int i = 0; i < this.audioClips.Length; i++)
		{
			GameObject gameObject = new GameObject("AudioPlayer");
			gameObject.transform.parent = base.gameObject.transform;
			gameObject.transform.position = base.gameObject.transform.position;
			this.audioSources[i] = gameObject.AddComponent<AudioSource>() as AudioSource;
			this.audioSources[i].clip = this.audioClips[i];
		}
		this.audioSources[0].volume = 0.7f;
	}

	// Token: 0x060000B8 RID: 184 RVA: 0x000081F0 File Offset: 0x000063F0
	private IEnumerator loadFireKey()
	{
		yield return new WaitForSeconds(0.5f);
		this.getFireKey = this.playerCtrl.fireKey;
		yield break;
	}

	// Token: 0x060000B9 RID: 185 RVA: 0x0000820C File Offset: 0x0000640C
	private void Update()
	{
		if (Input.GetButtonDown(this.getFireKey) && this.playerCtrl.canShoot)
		{
			facingRight = playerCtrl.facingRight ? 1 : -1;
			if (!this.isPunching)
			{
				if (!canPunch)
				{
					if (Time.time > this.nextFire && this.playerCtrl.ammo > 0)
					{
						this.nextFire = Time.time + this.fireRate;
						this.playerCtrl.ammo--;
						this.guiAmmo.setAmmo();
						if (this.playerCtrl.ammo == 0)
						{
							this.anim.SetBool("armed", false);
						}
						this.audioSources[0].Play();
						if (!this.hitWall)
						{
							if (this.facingRight == 1)
							{
								Rigidbody2D rigidbody2D = Object.Instantiate(this.projectile, base.transform.position, Quaternion.Euler(new Vector3(0f, 0f, 0f))) as Rigidbody2D;
								this.gunblastInstance = Object.Instantiate(this.gunblast, new Vector2(base.transform.position.x + 0.45f, base.transform.position.y + 0.05f), Quaternion.Euler(new Vector3(0f, 0f, 0f))) as Transform;
								this.gunblastInstance.transform.parent = base.gameObject.transform;
								rigidbody2D.velocity = new Vector2(this.speed, 0f);
								rigidbody2D.GetComponent<Projectile>().PlayerID = this.playerID;
							}
							else
							{
								Rigidbody2D rigidbody2D2 = Object.Instantiate(this.projectile, base.transform.position, Quaternion.Euler(new Vector3(0f, 0f, 180f))) as Rigidbody2D;
								this.gunblastInstance = Object.Instantiate(this.gunblast, new Vector2(base.transform.position.x - 0.45f, base.transform.position.y + 0.05f), Quaternion.Euler(new Vector3(0f, 0f, 180f))) as Transform;
								this.gunblastInstance.transform.parent = base.gameObject.transform;
								rigidbody2D2.velocity = new Vector2(-this.speed, 0f);
								rigidbody2D2.GetComponent<Projectile>().PlayerID = this.playerID;
							}
							Object.Destroy(this.gunblastInstance.gameObject, 0.1f);
						}
						base.StartCoroutine(this.playGunlight());
					}
				}
				else
				{
					this.anim.SetTrigger("Melee");
					base.StartCoroutine(punchDelay());
				}
			}

        }
		this.hitWall = Physics2D.Linecast(base.transform.position, base.transform.parent.position, 1 << LayerMask.NameToLayer("Ground"));
	}

	


    private void OnTriggerEnter2D(Collider2D collision)
    {
		if (collision.name == "_touchColl" || collision.tag == "Punchable")
		{
			canPunch = true;
		}
	}

    // Token: 0x060000BB RID: 187 RVA: 0x0000862C File Offset: 0x0000682C
    private void OnTriggerExit2D(Collider2D col)
	{
		if (col.name == "_touchColl" || col.tag == "Punchable")
		{
			canPunch = false;
		}
	}

	// Token: 0x060000BC RID: 188 RVA: 0x00008660 File Offset: 0x00006860
	public void gunMelee()
	{
		Vector2 vector = new Vector2(base.transform.position.x - 1f, base.transform.position.y + 2f);
		Vector2 vector2 = new Vector2(base.transform.position.x + 1f, base.transform.position.y - 2f);
		Collider2D[] array = Physics2D.OverlapAreaAll(vector, vector2);
		foreach (Collider2D collider2D in array)
		{
			if (collider2D && this.playerCtrl.canShoot)
			{
				if (collider2D.tag == "Player" && collider2D.transform != base.transform.parent.parent)
				{
					collider2D.GetComponent<PlayerLife>().Hurt(this.playerCtrl.playerID, (float)this.punchPower);
					collider2D.GetComponent<Rigidbody2D>().AddForce(new Vector2((float)(500 * this.facingRight), 0f));
				}
				else
				{
					collider2D.SendMessage("moveObject", this.facingRight, SendMessageOptions.DontRequireReceiver);
					collider2D.SendMessage("damageObject", this.punchPower, SendMessageOptions.DontRequireReceiver);
				}
				if (collider2D.tag == "Punchable")
				{
					this.audioSources[1].Play();
				}
			}
		}
		this.isPunching = false;
	}

	// Token: 0x060000BD RID: 189 RVA: 0x000087F8 File Offset: 0x000069F8
	public IEnumerator punchDelay()
	{
		isPunching = true;
		yield return new WaitForSeconds(0.3f);
		this.isPunching = false;
	}

	// Token: 0x060000BE RID: 190 RVA: 0x00008814 File Offset: 0x00006A14
	private IEnumerator playGunlight()
	{
		this.gunLight.transform.rotation = Quaternion.Euler(new Vector3(0f, 30f * (float)this.facingRight, 0f));
		this.gunLight.enabled = true;
		yield return new WaitForSeconds(0.05f);
		this.gunLight.enabled = false;
		yield break;
	}

	// Token: 0x040000D5 RID: 213
	public Rigidbody2D projectile;

	// Token: 0x040000D6 RID: 214
	public Transform gunblast;

	// Token: 0x040000D7 RID: 215
	public Light gunLight;

	// Token: 0x040000D8 RID: 216
	private Transform gunblastInstance;

	// Token: 0x040000D9 RID: 217
	public float speed = 20f;

	// Token: 0x040000DA RID: 218
	public float fireRate = 0.5f;

	// Token: 0x040000DB RID: 219
	private float nextFire;

	// Token: 0x040000DC RID: 220
	public int punchPower = 10;

	// Token: 0x040000DD RID: 221
	private int playerID;

	// Token: 0x040000DE RID: 222
	private PlayerControls playerCtrl;

	// Token: 0x040000DF RID: 223
	private string getFireKey;

	// Token: 0x040000E0 RID: 224
	public Animator anim;

	// Token: 0x040000E1 RID: 225
	public GuiAmmo guiAmmo;

	// Token: 0x040000E2 RID: 226
	public bool modeMelee;

	// Token: 0x040000E3 RID: 227
	public int facingRight = 1;

	// Token: 0x040000E4 RID: 228
	public bool isPunching;

	private bool canPunch;

	// Token: 0x040000E5 RID: 229
	public bool hitWall;

	// Token: 0x040000E6 RID: 230
	public AudioClip[] audioClips;

	// Token: 0x040000E7 RID: 231
	private AudioSource[] audioSources;
}
