﻿using System;
using System.Collections;
using UnityEngine;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;

// Token: 0x02000016 RID: 22
public class lightFlicker : MonoBehaviour
{
	// Token: 0x06000082 RID: 130 RVA: 0x00004C80 File Offset: 0x00002E80
	private void Start()
	{
		if (this.LightsToFlicker[0] != null)
		{
			for (int i = 0; i < this.LightsToFlicker.Length; i++)
			{
				this.LightsMax[i] = this.LightsToFlicker[i].intensity;
			}
		}
		this.setTimeList();
	}

	// Token: 0x06000083 RID: 131 RVA: 0x00004CD4 File Offset: 0x00002ED4
	private void setTimeList()
	{
		this.nrIntervals = 0;
		float num = this.durationSec;
		while (num > 0f)
		{
			this.TimeList[this.nrIntervals] = (float)Random.Range(1, 15) * (0.1f / this.intensity);
			num -= this.TimeList[this.nrIntervals];
			this.nrIntervals++;
		}
		base.StartCoroutine(this.flicker());
	}

	// Token: 0x06000084 RID: 132 RVA: 0x00004D50 File Offset: 0x00002F50
	private IEnumerator flicker()
	{
		for (int i = 0; i <= this.nrIntervals; i++)
		{
			yield return new WaitForSeconds(this.TimeList[i]);
			float lightVal = (float)Random.Range(-3, 0);
			for (int j = 0; j < this.LightsToFlicker.Length; j++)
			{
				this.LightsToFlicker[j].intensity = this.LightsMax[j] + lightVal;
			}
			for (int k = 0; k < this.SpritesToFlicker.Length; k++)
			{
				this.SpritesToFlicker[k].color = new Color(1.3f + lightVal / 3f, 1.3f + lightVal / 3f, 1.3f + lightVal / 3f, 1f);
			}
		}
		base.StartCoroutine(this.flicker());
		yield break;
	}

	// Token: 0x0400007C RID: 124
	public Light[] LightsToFlicker;

	// Token: 0x0400007D RID: 125
	public SpriteRenderer[] SpritesToFlicker;

	// Token: 0x0400007E RID: 126
	private float[] LightsMax = new float[20];

	// Token: 0x0400007F RID: 127
	private float colVal;

	// Token: 0x04000080 RID: 128
	public float durationSec = 1f;

	// Token: 0x04000081 RID: 129
	public float intensity = 1f;

	// Token: 0x04000082 RID: 130
	private bool makeBrighter = true;

	// Token: 0x04000083 RID: 131
	private float[] TimeList = new float[50];

	// Token: 0x04000084 RID: 132
	private int nrIntervals;

	// Token: 0x04000085 RID: 133
	private float mEndTime;

	// Token: 0x04000086 RID: 134
	private float mStartTime;
}
