﻿using System;
using System.Collections;
using UnityEngine;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;

// Token: 0x0200000A RID: 10
public class BarrelStand : MonoBehaviour
{
	// Token: 0x0600002F RID: 47 RVA: 0x00002E38 File Offset: 0x00001038
	private void Start()
	{
	}

	// Token: 0x06000030 RID: 48 RVA: 0x00002E3C File Offset: 0x0000103C
	private void Update()
	{
	}

	// Token: 0x06000031 RID: 49 RVA: 0x00002E40 File Offset: 0x00001040
	private void OnTriggerEnter2D(Collider2D col)
	{
		if (col.tag == "Projectile")
		{
			this.projSpeed = col.GetComponent<Rigidbody2D>().velocity.x;
			this.releaseBarrel();
		}
	}

	// Token: 0x06000032 RID: 50 RVA: 0x00002E84 File Offset: 0x00001084
	public void releaseBarrel()
	{
		if (!this.isBusy)
		{
			this.isBusy = true;
			if (this.projSpeed > 0f)
			{
				this.rollDir = 1;
			}
			else if (this.projSpeed < 0f)
			{
				this.rollDir = -1;
			}
			Rigidbody2D rigidbody2D = Object.Instantiate(this.barrelRoll, base.transform.position, Quaternion.Euler(new Vector3(0f, 0f, 0f))) as Rigidbody2D;
			rigidbody2D.GetComponent<Barrel>().lifetime = this.rollLifetime;
			rigidbody2D.GetComponent<Barrel>().health = this.health;
			this.toggleBarrel(false);
			int num = Random.Range(-5, 5);
			rigidbody2D.velocity = new Vector2(this.rollspeed * (float)this.rollDir + (float)num, 0f);
			if (this.respawnTime != 0f)
			{
				base.StartCoroutine(this.waitForRespawn());
			}
		}
	}

	// Token: 0x06000033 RID: 51 RVA: 0x00002F7C File Offset: 0x0000117C
	public void toggleBarrel(bool makeVisible)
	{
		Collider2D[] components = base.transform.GetComponents<Collider2D>();
		foreach (Collider2D collider2D in components)
		{
			collider2D.enabled = makeVisible;
		}
		base.transform.GetComponent<SpriteRenderer>().enabled = makeVisible;
	}

	// Token: 0x06000034 RID: 52 RVA: 0x00002FC8 File Offset: 0x000011C8
	public IEnumerator waitForRespawn()
	{
		yield return new WaitForSeconds(this.respawnTime);
		this.isBusy = false;
		this.toggleBarrel(true);
		yield break;
	}

	// Token: 0x06000035 RID: 53 RVA: 0x00002FE4 File Offset: 0x000011E4
	public void moveObject(int direction)
	{
		this.projSpeed = (float)(direction * 5);
		this.releaseBarrel();
	}

	// Token: 0x06000036 RID: 54 RVA: 0x00002FF8 File Offset: 0x000011F8
	public void killObject()
	{
		Vector2 vector = base.transform.position;
		this.toggleBarrel(false);
		if (this.respawnTime != 0f)
		{
			base.StartCoroutine(this.waitForRespawn());
		}
		GameObject.Find("_MainScript").GetComponent<MainFX>().explode(vector);
	}

	// Token: 0x04000022 RID: 34
	[HideInInspector]
	public int rollDir;

	// Token: 0x04000023 RID: 35
	public float rollspeed = 10f;

	// Token: 0x04000024 RID: 36
	public float respawnTime = 7f;

	// Token: 0x04000025 RID: 37
	public float rollLifetime = 7f;

	// Token: 0x04000026 RID: 38
	public float health = 50f;

	// Token: 0x04000027 RID: 39
	public bool isBusy;

	// Token: 0x04000028 RID: 40
	public float projSpeed;

	// Token: 0x04000029 RID: 41
	public Rigidbody2D barrelRoll;
}
