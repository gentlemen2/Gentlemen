﻿using System;
using System.Collections;
using UnityEngine;

// Token: 0x02000022 RID: 34
public class PlayerHealthbar : MonoBehaviour
{
	// Token: 0x060000DD RID: 221 RVA: 0x00009444 File Offset: 0x00007644
	private void Awake()
	{
		this.healthBar = base.GetComponent<SpriteRenderer>();
		this.healthScale = this.healthBar.transform.localScale;
	}

	// Token: 0x060000DE RID: 222 RVA: 0x00009474 File Offset: 0x00007674
	public void UpdateHealthBar(float health)
	{
		if (this.healthBar != null)
		{
			this.healthBar.material.color = new Color(1f, 0f, 0f, 1f);
			base.StartCoroutine(this.setColor(health));
		}
	}

	// Token: 0x060000DF RID: 223 RVA: 0x000094CC File Offset: 0x000076CC
	private IEnumerator setColor(float health)
	{
		yield return new WaitForSeconds(0.1f);
		this.localHealth = health;
		this.healthBar.material.color = Color.Lerp(Color.green, Color.red, 1f - health * 0.01f);
		this.healthBar.transform.localScale = new Vector3(this.healthScale.x * health * 0.01f, 1f, 1f);
		yield break;
	}

	// Token: 0x04000111 RID: 273
	private SpriteRenderer healthBar;

	// Token: 0x04000112 RID: 274
	private Vector3 healthScale;

	// Token: 0x04000113 RID: 275
	private Vector3 healthPos;

	// Token: 0x04000114 RID: 276
	public float localHealth;
}
