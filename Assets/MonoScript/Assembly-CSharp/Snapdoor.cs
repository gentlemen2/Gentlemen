﻿using System;
using System.Collections;
using UnityEngine;

// Token: 0x02000029 RID: 41
public class Snapdoor : MonoBehaviour
{
	// Token: 0x0600010D RID: 269 RVA: 0x0000A44C File Offset: 0x0000864C
	private void Start()
	{
		this.audioSources = new AudioSource[this.audioClips.Length];
		for (int i = 0; i < this.audioClips.Length; i++)
		{
			GameObject gameObject = new GameObject("AudioPlayer");
			gameObject.transform.parent = base.gameObject.transform;
			this.audioSources[i] = gameObject.AddComponent<AudioSource>() as AudioSource;
			this.audioSources[i].clip = this.audioClips[i];
		}
		this.closedSpritePos = this.spriteDoor.position;
		this.closedDoor = this.spriteDoor.GetComponent<SpriteRenderer>().sprite;
		if (this.opensToLeft)
		{
			this.doorOffset *= -1f;
			Vector3 localScale = this.spriteDoor.localScale;
			localScale.x *= -1f;
			this.spriteDoor.localScale = localScale;
		}
		this.openSpritePos = new Vector2(this.spriteDoor.position.x + this.doorOffset, this.spriteDoor.position.y);
		if (this.isOpen)
		{
			this.isOpen = !this.isOpen;
			this.toggleDoor(false);
		}
	}

	// Token: 0x0600010E RID: 270 RVA: 0x0000A5A4 File Offset: 0x000087A4
	private void OnTriggerStay2D(Collider2D col)
	{
		if (col.tag == "Player")
		{
			PlayerControls component = col.GetComponent<PlayerControls>();
			PlayerLife component2 = col.GetComponent<PlayerLife>();
			if (component.activate == 1 && !this.isBusy && !component2.killed && component.grounded)
			{
				this.isBusy = true;
				this.toggleDoor(true);
				base.StartCoroutine("pauseDoor");
			}
		}
	}

	// Token: 0x0600010F RID: 271 RVA: 0x0000A61C File Offset: 0x0000881C
	private void toggleDoor(bool playSnd = true)
	{
		this.spriteDoor.GetComponent<SpriteRenderer>().sprite = ((!this.isOpen) ? this.openedDoor : this.closedDoor);
		this.spriteDoor.position = ((!this.isOpen) ? this.openSpritePos : this.closedSpritePos);
		base.transform.gameObject.layer = ((!this.isOpen) ? 11 : 8);
		Collider2D[] components = base.transform.GetComponents<Collider2D>();
		foreach (Collider2D collider2D in components)
		{
			if (!collider2D.isTrigger)
			{
				collider2D.enabled = this.isOpen;
			}
		}
		if (playSnd)
		{
			int num = ((!this.isOpen) ? 0 : 1);
			this.audioSources[num].Play();
		}
		this.isOpen = !this.isOpen;
	}

	// Token: 0x06000110 RID: 272 RVA: 0x0000A718 File Offset: 0x00008918
	private IEnumerator pauseDoor()
	{
		yield return new WaitForSeconds(this.pauseTime);
		this.isBusy = false;
		yield break;
	}

	// Token: 0x04000142 RID: 322
	public bool isOpen;

	// Token: 0x04000143 RID: 323
	public bool opensToLeft;

	// Token: 0x04000144 RID: 324
	private bool isBusy;

	// Token: 0x04000145 RID: 325
	public float pauseTime = 0.4f;

	// Token: 0x04000146 RID: 326
	public Transform spriteDoor;

	// Token: 0x04000147 RID: 327
	public float doorOffset = 1.2f;

	// Token: 0x04000148 RID: 328
	private Vector2 openSpritePos;

	// Token: 0x04000149 RID: 329
	private Vector2 closedSpritePos;

	// Token: 0x0400014A RID: 330
	public Sprite openedDoor;

	// Token: 0x0400014B RID: 331
	private Sprite closedDoor;

	// Token: 0x0400014C RID: 332
	public AudioClip[] audioClips;

	// Token: 0x0400014D RID: 333
	private AudioSource[] audioSources;
}
