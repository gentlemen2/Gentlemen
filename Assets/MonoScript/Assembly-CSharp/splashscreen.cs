﻿using System;
using UnityEngine;

// Token: 0x0200001C RID: 28
public class splashscreen : MonoBehaviour
{
	// Token: 0x060000B3 RID: 179 RVA: 0x00007FE4 File Offset: 0x000061E4
	private void Start()
	{
		this.timeOut = Time.time + 4f;
	}

	// Token: 0x060000B4 RID: 180 RVA: 0x00007FF8 File Offset: 0x000061F8
	private void Update()
	{
		if (Time.time > this.timeOut)
		{
			Application.LoadLevel("MainMenu");
		}
	}

	// Token: 0x060000B5 RID: 181 RVA: 0x00008014 File Offset: 0x00006214
	private void OnGUI()
	{
		GUI.color = new Color(1f, 1f, 1f, 0f);
		if (GUI.Button(new Rect(0f, 0f, (float)Screen.width, (float)Screen.height), string.Empty))
		{
			Application.LoadLevel("MainMenu");
		}
	}

	// Token: 0x040000D4 RID: 212
	private float timeOut;
}
