﻿using System;
using System.Collections;
using UnityEngine;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;

// Token: 0x0200002B RID: 43
public class Warplift : MonoBehaviour
{
	// Token: 0x06000118 RID: 280 RVA: 0x0000A984 File Offset: 0x00008B84
	private void Start()
	{
		if (this.actFloor == 0)
		{
			MonoBehaviour.print("WARNUNG: Etage nicht gesetzt von: " + base.name);
		}
		this.floorCount = this.doors.Length;
		this.initLift();
		this.changeLights();
		this.audioSources = new AudioSource[this.audioClips.Length];
		for (int i = 0; i < this.audioClips.Length; i++)
		{
			GameObject gameObject = new GameObject("AudioPlayer");
			gameObject.transform.parent = base.gameObject.transform;
			this.audioSources[i] = gameObject.AddComponent<AudioSource>() as AudioSource;
			this.audioSources[i].clip = this.audioClips[i];
		}
		this.audioSources[2].loop = true;
		this.audioSources[1].volume = 0.5f;
		this.audioSources[2].volume = 0.8f;
	}

	// Token: 0x06000119 RID: 281 RVA: 0x0000AA78 File Offset: 0x00008C78
	private void Update()
	{
	}

	// Token: 0x0600011A RID: 282 RVA: 0x0000AA7C File Offset: 0x00008C7C
	public void callLift(int newFloor)
	{
		if (!this.isBusy)
		{
			this.nextFloor = newFloor;
			base.StartCoroutine(moveLift());
		}
	}

	// Token: 0x0600011B RID: 283 RVA: 0x0000AA9C File Offset: 0x00008C9C
	public void enterLift(Transform player, Transform enterDoor, int goUp)
	{
		if (!this.isBusy)
		{
			bool flag = false;
			if (goUp == 1 && this.actFloor != this.floorCount)
			{
				this.nextFloor = this.actFloor + 1;
				flag = true;
			}
			else if (goUp == -1 && this.actFloor != 1)
			{
				this.nextFloor = this.actFloor - 1;
				flag = true;
			}
			if (flag)
			{
				this.actPlayer = player;
				this.yDiff = this.actPlayer.transform.position.y - enterDoor.position.y;
				this.actPlayer.GetComponent<PlayerTransform>().makeInvisible(true);
				this.isFull = true;
				base.StartCoroutine(moveLift());
			}
		}
	}

	// Token: 0x0600011C RID: 284 RVA: 0x0000AB64 File Offset: 0x00008D64
	public IEnumerator moveLift()
	{
		Transform openedDoor = base.transform.Find("Warpliftdoor " + this.actFloor);
		this.openDoor(false, openedDoor);
		this.audioSources[1].Play();
		this.isBusy = true;
		this.audioSources[2].Play();
		yield return new WaitForSeconds((float)this.speed * 0.7f);
		this.actFloor = this.nextFloor;
		this.changeDoors(this.actFloor);
		yield return new WaitForSeconds((float)this.speed * 0.3f);
		this.changeLights();
		this.audioSources[0].Play();
		this.audioSources[2].Stop();
		yield return new WaitForSeconds(0.3f);
		this.audioSources[1].Play();
		yield return new WaitForSeconds(0.3f);
		if (this.actPlayer)
		{
			this.exitPlayer();
		}
		this.isFull = false;
		yield return new WaitForSeconds(0.4f);
		this.isBusy = false;
		yield break;
	}

	// Token: 0x0600011D RID: 285 RVA: 0x0000AB80 File Offset: 0x00008D80
	private void changeLights()
	{
		foreach (GameObject gameObject in this.doors)
		{
			for (int j = 1; j <= this.floorCount; j++)
			{
				Transform transform = gameObject.transform.Find("warpliftlight" + j);
				transform.GetComponent<SpriteRenderer>().color = new Color(0.2f, 0.2f, 0.2f, 1f);
				if (j == this.actFloor)
				{
					transform.GetComponent<SpriteRenderer>().color = new Color(1f, 0f, 0f, 1f);
				}
			}
		}
	}

	// Token: 0x0600011E RID: 286 RVA: 0x0000AC38 File Offset: 0x00008E38
	public void changeDoors(int floor = 0)
	{
		if (floor != 0)
		{
			Transform transform = base.transform.Find("Warpliftdoor " + floor);
			this.openDoor(true, transform);
		}
	}

	// Token: 0x0600011F RID: 287 RVA: 0x0000AC70 File Offset: 0x00008E70
	private void openDoor(bool makeOpen, Transform door)
	{
		Animator component = door.GetComponent<Animator>();
		if (this.actFloor != this.nextFloor)
		{
			this.direction = this.nextFloor - this.actFloor;
		}
		if (makeOpen)
		{
			component.SetInteger("close", 0);
			component.SetInteger("open", this.direction);
		}
		else
		{
			component.SetInteger("open", 0);
			component.SetInteger("close", this.direction);
		}
	}

	// Token: 0x06000120 RID: 288 RVA: 0x0000ACF0 File Offset: 0x00008EF0
	private void exitPlayer()
	{
		Transform transform = base.transform.Find("Warpliftdoor " + this.actFloor);
		this.actPlayer.transform.position = new Vector2(transform.transform.position.x, transform.transform.position.y + this.yDiff);
		this.actPlayer.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
		this.actPlayer.GetComponent<PlayerTransform>().makeInvisible(false);
		this.actPlayer = null;
	}

	// Token: 0x06000121 RID: 289 RVA: 0x0000AD94 File Offset: 0x00008F94
	private void initLift()
	{
		int num = 1;
		foreach (GameObject gameObject in this.doors)
		{
			if (gameObject == null)
			{
				MonoBehaviour.print("Fehlende Tür in Warplift " + base.name);
			}
			int doorID = gameObject.transform.GetComponent<WarpliftDoor>().doorID;
			if (num != doorID)
			{
				MonoBehaviour.print(string.Concat(new object[] { "Lifttür ", num, " von Lift ", base.name, " hat falsche doorID" }));
			}
			num++;
			Transform transform = gameObject.transform.Find("warpliftlight");
			transform.name += 1;
			float x = transform.GetComponent<SpriteRenderer>().bounds.extents.x;
			float num2 = transform.transform.position.x - (x * (float)this.floorCount + this.lightDist * (float)(this.floorCount - 1) * 0.07f) / 2f;
			transform.transform.position = new Vector2(num2, transform.transform.position.y);
			for (int j = 1; j < this.floorCount; j++)
			{
				Vector2 vector = new Vector2(num2 + x * (float)j * this.lightDist, transform.transform.position.y);
				Transform transform2 = Object.Instantiate(transform, vector, Quaternion.Euler(new Vector3(0f, 0f, 0f))) as Transform;
				transform2.transform.parent = gameObject.transform;
				transform2.name = "warpliftlight" + (j + 1);
			}
		}
		Transform transform3 = base.transform.Find("Warpliftdoor " + this.actFloor);
		transform3.GetComponent<Animator>().SetInteger("open", -1);
	}

	// Token: 0x04000159 RID: 345
	public int speed = 2;

	// Token: 0x0400015A RID: 346
	public int actFloor;

	// Token: 0x0400015B RID: 347
	[HideInInspector]
	public int nextFloor;

	// Token: 0x0400015C RID: 348
	private int direction;

	// Token: 0x0400015D RID: 349
	public GameObject[] doors;

	// Token: 0x0400015E RID: 350
	public Transform actPlayer;

	// Token: 0x0400015F RID: 351
	private float yDiff;

	// Token: 0x04000160 RID: 352
	[HideInInspector]
	public int floorCount;

	// Token: 0x04000161 RID: 353
	[HideInInspector]
	public bool isBusy;

	// Token: 0x04000162 RID: 354
	[HideInInspector]
	public bool isFull;

	// Token: 0x04000163 RID: 355
	private float lightDist = 2.5f;

	// Token: 0x04000164 RID: 356
	public AudioClip[] audioClips;

	// Token: 0x04000165 RID: 357
	private AudioSource[] audioSources;
}
