﻿using System;
using UnityEngine;

// Token: 0x02000015 RID: 21
public class lightDim : MonoBehaviour
{
	// Token: 0x0600007D RID: 125 RVA: 0x00004A50 File Offset: 0x00002C50
	private void Awake()
	{
		this.setTimer();
	}

	// Token: 0x0600007E RID: 126 RVA: 0x00004A58 File Offset: 0x00002C58
	private void Start()
	{
		if (this.LightsToDim[0] != null)
		{
			for (int i = 0; i < this.LightsToDim.Length; i++)
			{
				this.LightsMax[i] = this.LightsToDim[i].intensity;
			}
		}
	}

	// Token: 0x0600007F RID: 127 RVA: 0x00004AA8 File Offset: 0x00002CA8
	private void Update()
	{
		if (this.LightsToDim[0] != null)
		{
			for (int i = 0; i < this.LightsToDim.Length; i++)
			{
				if (this.makeBrighter)
				{
					this.LightsToDim[i].intensity = Mathf.InverseLerp(this.mStartTime, this.mEndTime, Time.time) * this.LightsMax[i];
				}
				else
				{
					this.LightsToDim[i].intensity = this.LightsMax[i] - Mathf.InverseLerp(this.mStartTime, this.mEndTime, Time.time) * this.LightsMax[i];
				}
			}
		}
		if (this.SpritesToDim[0] != null)
		{
			if (this.makeBrighter)
			{
				this.colVal = 0.3f + Mathf.InverseLerp(this.mStartTime, this.mEndTime, Time.time);
			}
			else
			{
				this.colVal = 1.3f - Mathf.InverseLerp(this.mStartTime, this.mEndTime, Time.time);
			}
			for (int j = 0; j < this.SpritesToDim.Length; j++)
			{
				this.SpritesToDim[j].color = new Color(this.colVal, this.colVal, this.colVal, 1f);
			}
		}
		if (Time.time > this.mEndTime)
		{
			this.setTimer();
			this.makeBrighter = !this.makeBrighter;
		}
	}

	// Token: 0x06000080 RID: 128 RVA: 0x00004C20 File Offset: 0x00002E20
	private void setTimer()
	{
		this.mStartTime = Time.time;
		this.mEndTime = this.mStartTime + this.durationSec;
	}

	// Token: 0x04000074 RID: 116
	public Light[] LightsToDim;

	// Token: 0x04000075 RID: 117
	public SpriteRenderer[] SpritesToDim;

	// Token: 0x04000076 RID: 118
	private float[] LightsMax = new float[20];

	// Token: 0x04000077 RID: 119
	private float colVal;

	// Token: 0x04000078 RID: 120
	public float durationSec = 5f;

	// Token: 0x04000079 RID: 121
	private bool makeBrighter = true;

	// Token: 0x0400007A RID: 122
	private float mEndTime;

	// Token: 0x0400007B RID: 123
	private float mStartTime;
}
