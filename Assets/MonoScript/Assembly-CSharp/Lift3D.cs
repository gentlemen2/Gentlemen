﻿using System;
using UnityEngine;

// Token: 0x02000027 RID: 39
public class Lift3D : MonoBehaviour
{
	// Token: 0x06000102 RID: 258 RVA: 0x0000A0A0 File Offset: 0x000082A0
	private void Start()
	{
		this.audioSources = new AudioSource[this.audioClips.Length];
		for (int i = 0; i < this.audioClips.Length; i++)
		{
			GameObject gameObject = new GameObject("AudioPlayer");
			gameObject.transform.parent = base.gameObject.transform;
			this.audioSources[i] = gameObject.AddComponent<AudioSource>() as AudioSource;
			this.audioSources[i].clip = this.audioClips[i];
		}
		this.audioSources[2].loop = true;
		base.transform.position = new Vector2(base.transform.position.x, this.yFloors[this.actFloor]);
	}

	// Token: 0x06000103 RID: 259 RVA: 0x0000A16C File Offset: 0x0000836C
	private void Update()
	{
		if (this.direction == 1)
		{
			if (base.transform.position.y > this.newY)
			{
				this.stopLift();
			}
		}
		else if (this.direction == -1 && base.transform.position.y < this.newY)
		{
			this.stopLift();
		}
	}

	// Token: 0x06000104 RID: 260 RVA: 0x0000A1E0 File Offset: 0x000083E0
	private void OnTriggerStay2D(Collider2D col)
	{
		if (col.name == "_headCollider")
		{
			Transform parent = col.transform.parent;
			int activate = parent.gameObject.GetComponent<PlayerControls>().activate;
			if (activate != 0)
			{
				if (activate < 0 && this.actFloor > 0)
				{
					int num = this.actFloor - 1;
					this.gotoFloor(num);
				}
				else if (activate > 0 && this.actFloor < this.yFloors.Length - 1)
				{
					int num2 = this.actFloor + 1;
					this.gotoFloor(num2);
				}
			}
		}
	}

	// Token: 0x06000105 RID: 261 RVA: 0x0000A27C File Offset: 0x0000847C
	public void switchOn(float floorNr)
	{
		int num = Mathf.FloorToInt(floorNr);
		this.gotoFloor(num);
	}

	// Token: 0x06000106 RID: 262 RVA: 0x0000A298 File Offset: 0x00008498
	public void gotoFloor(int newFloor)
	{
		if (base.GetComponent<Rigidbody2D>().velocity.y == 0f && this.direction == 0 && this.actFloor != newFloor)
		{
			this.newY = this.yFloors[newFloor];
			int num;
			if (base.transform.position.y < this.newY)
			{
				num = 1;
			}
			else
			{
				num = -1;
			}
			base.GetComponent<Rigidbody2D>().velocity = new Vector2(0f, (float)(this.speed * num));
			this.actFloor = newFloor;
			this.direction = num;
			this.audioSources[2].Play();
		}
	}

	// Token: 0x06000107 RID: 263 RVA: 0x0000A34C File Offset: 0x0000854C
	public void stopLift()
	{
		this.audioSources[2].Stop();
		base.GetComponent<Rigidbody2D>().velocity = new Vector2(0f, 0f);
		this.direction = 0;
	}

	// Token: 0x04000137 RID: 311
	public float[] yFloors;

	// Token: 0x04000138 RID: 312
	public int speed = 2;

	// Token: 0x04000139 RID: 313
	public int actFloor;

	// Token: 0x0400013A RID: 314
	private PlayerControls playerCtrl;

	// Token: 0x0400013B RID: 315
	private string getVertAxis;

	// Token: 0x0400013C RID: 316
	private float newY;

	// Token: 0x0400013D RID: 317
	public int direction;

	// Token: 0x0400013E RID: 318
	public AudioClip[] audioClips;

	// Token: 0x0400013F RID: 319
	private AudioSource[] audioSources;
}
