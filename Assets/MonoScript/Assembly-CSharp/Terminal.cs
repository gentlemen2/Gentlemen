﻿using System;
using UnityEngine;

// Token: 0x02000005 RID: 5
public class Terminal : MonoBehaviour
{
	// Token: 0x06000015 RID: 21 RVA: 0x00002618 File Offset: 0x00000818
	private void Start()
	{
		if (this.playerID == 0)
		{
			MonoBehaviour.print("Terminal hat noch keine PlayerID: " + base.name);
		}
		this.mainScript = GameObject.Find("_MainScript").GetComponent<Main>();
		this.myPlayer = GameObject.Find("Player" + this.playerID);
		this.myPlGames = this.myPlayer.GetComponent<PlayerGames>();
	}

	// Token: 0x06000016 RID: 22 RVA: 0x0000268C File Offset: 0x0000088C
	private void Update()
	{
	}

	// Token: 0x06000017 RID: 23 RVA: 0x00002690 File Offset: 0x00000890
	public void setTermColor(Color myColor)
	{
		base.transform.GetComponent<SpriteRenderer>().color = myColor;
	}

	// Token: 0x06000018 RID: 24 RVA: 0x000026A4 File Offset: 0x000008A4
	private void OnTriggerEnter2D(Collider2D col)
	{
		if (col.name == this.myPlayer.name && this.myPlGames.nrDisks > 0)
		{
			DiskMaster component = GameObject.Find("DiskMaster").GetComponent<DiskMaster>();
			component.actNrDisks -= this.myPlGames.nrDisks;
			component.requestDisk();
			this.mainScript.setDisks(this.playerID, this.myPlGames.nrDisks);
			this.myPlGames.nrDisks = 0;
			this.myPlGames.changeDisks();
			base.GetComponent<AudioSource>().Play();
		}
	}

	// Token: 0x0400000E RID: 14
	public int playerID;

	// Token: 0x0400000F RID: 15
	private GameObject myPlayer;

	// Token: 0x04000010 RID: 16
	private PlayerGames myPlGames;

	// Token: 0x04000011 RID: 17
	private Main mainScript;
}
