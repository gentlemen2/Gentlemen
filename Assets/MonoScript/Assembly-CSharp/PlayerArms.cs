﻿using System;
using UnityEngine;

// Token: 0x0200001E RID: 30
public class PlayerArms : MonoBehaviour
{

	private Gun gun;
	// Token: 0x060000C0 RID: 192 RVA: 0x00008838 File Offset: 0x00006A38
	private void Start()
	{
		gun = transform.Find("Gun").GetComponent<Gun>();
	}

	// Token: 0x060000C2 RID: 194 RVA: 0x00008840 File Offset: 0x00006A40
	public void meleeHurt()
	{
		gun.gunMelee();
	}
}
