﻿using System;
using UnityEngine;

// Token: 0x02000007 RID: 7
public class gapKill : MonoBehaviour
{
	// Token: 0x0600001D RID: 29 RVA: 0x000028CC File Offset: 0x00000ACC
	private void OnTriggerEnter2D(Collider2D col)
	{
		if (col.tag == "Player")
		{
			PlayerLife component = col.gameObject.GetComponent<PlayerLife>();
			if (!component.killed)
			{
				component.killPlayer(0);
			}
		}
	}
}
