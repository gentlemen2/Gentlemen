﻿using System;
using System.Collections;
using UnityEngine;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;

// Token: 0x02000009 RID: 9
public class Barrel : MonoBehaviour
{
	// Token: 0x06000024 RID: 36 RVA: 0x00002A60 File Offset: 0x00000C60
	private void Start()
	{
		this.startPos = base.transform.position;
		base.StartCoroutine(this.makeHurt());
		if (this.lifetime > 0f)
		{
			base.StartCoroutine(this.autoDestruct());
		}
	}

	// Token: 0x06000025 RID: 37 RVA: 0x00002AB0 File Offset: 0x00000CB0
	private void Update()
	{
	}

	// Token: 0x06000026 RID: 38 RVA: 0x00002AB4 File Offset: 0x00000CB4
	private void OnCollisionEnter2D(Collision2D col)
	{
		float num = base.GetComponent<Rigidbody2D>().velocity.x;
		float num2 = base.GetComponent<Rigidbody2D>().velocity.y;
		if (num < 0f)
		{
			num *= -1f;
		}
		if (num2 < 0f)
		{
			num2 *= -1f;
		}
		float num3 = num + num2;
		float num4 = num * num2;
		if (num3 > 10f && num2 > 1f && !this.sndBusy)
		{
			base.GetComponent<AudioSource>().Play();
			base.StartCoroutine("pauseSnd");
		}
		if (num3 > 25f)
		{
			GameObject.Find("_MainScript").GetComponent<MainFX>().StartCoroutine("shakeCam");
		}
		if (col.gameObject.tag == "Player" && this.canHurt)
		{
			float x = col.gameObject.GetComponent<Rigidbody2D>().velocity.x;
			float x2 = col.gameObject.GetComponent<Transform>().position.x;
			base.GetComponent<Rigidbody2D>().velocity = new Vector2(base.GetComponent<Rigidbody2D>().velocity.x + x * -2f, base.GetComponent<Rigidbody2D>().velocity.y);
			if (num3 > 0.1f)
			{
				this.damageObject(num3);
				col.gameObject.GetComponent<PlayerLife>().Hurt(0, num3 * 5f);
				this.canHurt = false;
				base.StartCoroutine(this.makeHurt());
			}
		}
		if (col.gameObject.tag == "Architecture" && num4 > 5f)
		{
			this.damageObject(num3 * 0.5f);
			if (num == 0f)
			{
				int num5 = Random.Range(-100, 100);
				base.GetComponent<Rigidbody2D>().AddForce(new Vector2((float)(num5 * 2), 0f));
				base.GetComponent<Rigidbody2D>().AddTorque((float)(-(float)num5));
			}
		}
	}

	// Token: 0x06000027 RID: 39 RVA: 0x00002CC4 File Offset: 0x00000EC4
	public IEnumerator startToHurt()
	{
		yield return new WaitForSeconds(0.5f);
		this.canHurt = true;
		yield break;
	}

	// Token: 0x06000028 RID: 40 RVA: 0x00002CE0 File Offset: 0x00000EE0
	public IEnumerator makeHurt()
	{
		yield return new WaitForSeconds(0.2f);
		this.canHurt = true;
		yield break;
	}

	// Token: 0x06000029 RID: 41 RVA: 0x00002CFC File Offset: 0x00000EFC
	public void damageObject(float damage)
	{
		this.health -= damage;
		base.transform.GetComponent<SpriteRenderer>().color = new Color(1f, 0f, 0f, 1f);
		base.StartCoroutine(this.defaultColor());
		if (this.health <= 0f)
		{
			this.killObject();
		}
	}

	// Token: 0x0600002A RID: 42 RVA: 0x00002D64 File Offset: 0x00000F64
	public void killObject()
	{
		Vector2 vector = base.transform.position;
		Object.Destroy(base.gameObject);
		GameObject.Find("_MainScript").GetComponent<MainFX>().explode(vector);
	}

	// Token: 0x0600002B RID: 43 RVA: 0x00002DA4 File Offset: 0x00000FA4
	public IEnumerator autoDestruct()
	{
		yield return new WaitForSeconds(this.lifetime);
		this.killObject();
		yield break;
	}

	// Token: 0x0600002C RID: 44 RVA: 0x00002DC0 File Offset: 0x00000FC0
	public IEnumerator defaultColor()
	{
		yield return new WaitForSeconds(0.1f);
		base.transform.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 1f);
		yield break;
	}

	// Token: 0x0600002D RID: 45 RVA: 0x00002DDC File Offset: 0x00000FDC
	public IEnumerator pauseSnd()
	{
		this.sndBusy = true;
		yield return new WaitForSeconds(0.3f);
		this.sndBusy = false;
		yield break;
	}

	// Token: 0x0400001B RID: 27
	private Vector2 startPos;

	// Token: 0x0400001C RID: 28
	public float health = 50f;

	// Token: 0x0400001D RID: 29
	public float respawnSec = 2f;

	// Token: 0x0400001E RID: 30
	public float lifetime = 7f;

	// Token: 0x0400001F RID: 31
	public int direction = 1;

	// Token: 0x04000020 RID: 32
	private bool canHurt;

	// Token: 0x04000021 RID: 33
	private bool sndBusy;
}
