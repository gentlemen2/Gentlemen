﻿using System;
using System.Collections;
using UnityEngine;

// Token: 0x02000021 RID: 33
public class PlayerHead : MonoBehaviour
{
	// Token: 0x060000D5 RID: 213 RVA: 0x00009280 File Offset: 0x00007480
	private void Start()
	{
		this.plCtrl = base.transform.root.GetComponent<PlayerControls>();
		this.plLife = base.transform.root.GetComponent<PlayerLife>();
	}

	// Token: 0x060000D6 RID: 214 RVA: 0x000092BC File Offset: 0x000074BC
	private void Update()
	{
	}

	// Token: 0x060000D7 RID: 215 RVA: 0x000092C0 File Offset: 0x000074C0
	private void OnTriggerStay2D(Collider2D col)
	{
		if (col.gameObject.tag == "Architecture")
		{
			bool grounded = this.plCtrl.grounded;
			if (col.transform.position.y < base.transform.position.y && grounded && !this.willDie && !this.plLife.killed)
			{
				MonoBehaviour.print("Squashed Head on " + col.name);
				this.willDie = true;
				base.StartCoroutine("killPlayer");
			}
		}
		if (col.gameObject.tag == "Water" && !this.willDie && !this.plLife.killed)
		{
			MonoBehaviour.print("Down water in " + col.name);
			this.willDie = true;
			base.StartCoroutine("drownPlayer");
		}
	}

	// Token: 0x060000D8 RID: 216 RVA: 0x000093C4 File Offset: 0x000075C4
	private void OnTriggerEnter2D(Collider2D col)
	{
		if (col.gameObject.tag == "Water")
		{
			base.GetComponent<AudioSource>().Play();
		}
	}

	// Token: 0x060000D9 RID: 217 RVA: 0x000093F8 File Offset: 0x000075F8
	private void OnTriggerExit2D(Collider2D col)
	{
		this.willDie = false;
	}

	// Token: 0x060000DA RID: 218 RVA: 0x00009404 File Offset: 0x00007604
	public IEnumerator killPlayer()
	{
		yield return new WaitForSeconds(0.4f);
		if (this.willDie)
		{
			this.plLife.killPlayer(-1);
			this.willDie = false;
		}
		yield break;
	}

	// Token: 0x060000DB RID: 219 RVA: 0x00009420 File Offset: 0x00007620
	public IEnumerator drownPlayer()
	{
		yield return new WaitForSeconds(5f);
		if (this.willDie)
		{
			this.plLife.killPlayer(0);
			this.willDie = false;
		}
		yield break;
	}

	// Token: 0x0400010E RID: 270
	public bool willDie;

	// Token: 0x0400010F RID: 271
	private PlayerControls plCtrl;

	// Token: 0x04000110 RID: 272
	private PlayerLife plLife;
}
