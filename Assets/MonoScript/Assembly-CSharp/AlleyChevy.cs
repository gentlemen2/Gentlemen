﻿using System;
using System.Collections;
using UnityEngine;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;

// Token: 0x02000002 RID: 2
public class AlleyChevy : MonoBehaviour
{
	// Token: 0x06000002 RID: 2 RVA: 0x00002128 File Offset: 0x00000328
	private void Start()
	{
		this.startPos = base.transform.position;
		this.setCar();
	}

	// Token: 0x06000003 RID: 3 RVA: 0x00002144 File Offset: 0x00000344
	private void Update()
	{
		if ((!this.busy && base.transform.position.x > this.stageBorder && this.direction == 1) || (base.transform.position.x < -this.stageBorder && this.direction == -1))
		{
			this.busy = true;
			base.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
			base.StartCoroutine("respawnCar");
		}
	}

	// Token: 0x06000004 RID: 4 RVA: 0x000021D4 File Offset: 0x000003D4
	public void moveCar()
	{
		float num = this.speed + Random.Range(-this.speedRange, this.speedRange);
		base.GetComponent<Rigidbody2D>().velocity = new Vector2(num * (float)this.direction, 0f);
	}

	// Token: 0x06000005 RID: 5 RVA: 0x0000221C File Offset: 0x0000041C
	public void setCar()
	{
		base.StopAllCoroutines();
		int num = this.direction;
		this.direction = Random.Range(-1, 1);
		if (this.direction == 0)
		{
			this.direction = 1;
		}
		base.transform.position = new Vector3(this.stageBorder * (float)(-(float)this.direction), base.transform.position.y, base.transform.position.z);
		Vector3 localScale = base.transform.localScale;
		localScale.x = (float)this.direction;
		base.transform.localScale = localScale;
		Light[] componentsInChildren = base.transform.GetComponentsInChildren<Light>();
		foreach (Light light in componentsInChildren)
		{
			light.transform.rotation = Quaternion.Euler(new Vector3(light.transform.eulerAngles.x, light.transform.eulerAngles.y * (float)this.direction * (float)num, 0f));
		}
		this.moveCar();
		base.StartCoroutine("makeBusy");
	}

	// Token: 0x06000006 RID: 6 RVA: 0x00002350 File Offset: 0x00000550
	private IEnumerator respawnCar()
	{
		float waitForRespawn = Random.Range(0f, this.maxRespawnTime + 1f);
		yield return new WaitForSeconds(waitForRespawn);
		this.setCar();
		yield break;
	}

	// Token: 0x06000007 RID: 7 RVA: 0x0000236C File Offset: 0x0000056C
	private IEnumerator makeBusy()
	{
		yield return new WaitForSeconds(1f);
		this.busy = false;
		yield break;
	}

	// Token: 0x04000001 RID: 1
	public float speed = 10f;

	// Token: 0x04000002 RID: 2
	public float speedRange = 3f;

	// Token: 0x04000003 RID: 3
	public float maxRespawnTime = 5f;

	// Token: 0x04000004 RID: 4
	public float stageBorder = 30f;

	// Token: 0x04000005 RID: 5
	public int direction = 1;

	// Token: 0x04000006 RID: 6
	private Vector3 startPos;

	// Token: 0x04000007 RID: 7
	private bool busy;
}
