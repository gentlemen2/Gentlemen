﻿using System;
using System.Collections;
using UnityEngine;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;

// Token: 0x02000023 RID: 35
public class PlayerLife : MonoBehaviour
{
	// Token: 0x060000E1 RID: 225 RVA: 0x00009514 File Offset: 0x00007714
	private void Start()
	{
		this.plCtrls = base.transform.root.GetComponent<PlayerControls>();
		this.plGames = base.transform.root.GetComponent<PlayerGames>();
		this.plTrans = base.transform.root.GetComponent<PlayerTransform>();
		this.mainScript = GameObject.Find("_MainScript").GetComponent<Main>();
		this.audioSources = new AudioSource[this.audioClips.Length];
		for (int i = 0; i < this.audioClips.Length; i++)
		{
			GameObject gameObject = new GameObject("AudioPlayer");
			gameObject.transform.parent = base.gameObject.transform;
			gameObject.transform.position = base.gameObject.transform.position;
			this.audioSources[i] = gameObject.AddComponent<AudioSource>() as AudioSource;
			this.audioSources[i].clip = this.audioClips[i];
		}
		if (this.mainScript.oneShotKiller)
		{
			this.health = 1f;
			Object.Destroy(base.transform.root.Find("_healthbar").GetComponent<SpriteRenderer>());
			Object.Destroy(base.transform.root.Find("_healthbarBG").GetComponent<SpriteRenderer>());
		}
		this.setHealth = this.health;
	}

	// Token: 0x060000E2 RID: 226 RVA: 0x00009674 File Offset: 0x00007874
	private void Update()
	{
	}

	// Token: 0x060000E3 RID: 227 RVA: 0x00009678 File Offset: 0x00007878
	public void Hurt(int killerID, float damage = 22f)
	{
		if (!this.invincible)
		{
			this.health -= damage;
			this.audioSources[0].Play();
		}
		if (this.health <= 0f)
		{
			this.killPlayer(killerID);
		}
		if (!this.killed)
		{
			this.plTrans.hurtColor();
			this.updateHealthbar();
			this.plTrans.StartCoroutine("StartResetColor");
		}
	}

	// Token: 0x060000E4 RID: 228 RVA: 0x000096F0 File Offset: 0x000078F0
	public void killPlayer(int killerID = 0)
	{
		this.plCtrls.canMove = false;
		this.plCtrls.canShoot = false;
		this.health = 0f;
		this.updateHealthbar();
		this.plTrans.hurtColor();
		bool flag = false;
		this.plCtrls.anim.SetFloat("Speed", 0f);
		if (killerID == -1)
		{
			this.plCtrls.anim.SetBool("Squashed", true);
		}
		if (killerID <= 0)
		{
			flag = true;
			killerID = this.plCtrls.playerID;
		}
		else
		{
			this.plCtrls.anim.SetBool("Squashed", false);
			base.GetComponent<Rigidbody2D>().isKinematic = true;
		}
		this.plCtrls.anim.SetBool("alive", false);
		base.transform.root.Find("Arms").GetComponent<Animator>().SetBool("alive", false);
		Main component = GameObject.Find("_MainScript").GetComponent<Main>();
		if (!this.killed)
		{
			component.setKills(killerID, flag);
			this.killed = true;
			if (this.plGames.hasLaptop)
			{
				if (base.transform.root.Find("Laptop") != null)
				{
					base.transform.root.Find("Laptop").GetComponent<Laptop>().loseLaptop();
				}
				this.plGames.hasLaptop = false;
			}
			this.plGames.leaveDisk();
			if (base.transform.root.Find("Bomb") != null)
			{
				base.transform.root.Find("Bomb").GetComponent<Timebomb>().explode();
			}
		}
		if (!component.gameOver)
		{
			base.StartCoroutine(resetPlayer());
		}
	}

	// Token: 0x060000E5 RID: 229 RVA: 0x000098C8 File Offset: 0x00007AC8
	public IEnumerator resetPlayer()
	{
		yield return new WaitForSeconds(0.3f);
		this.audioSources[1].Play();
		yield return new WaitForSeconds(0.7f);
		this.plTrans.resetColor();
		base.GetComponent<Rigidbody2D>().isKinematic = true;
		this.plCtrls.ammo = this.plCtrls.startAmmo;
		GameObject.Find("GuiAmmo").GetComponent<GuiAmmo>().setAmmo();
		base.transform.root.Find("Arms").GetComponent<Animator>().SetBool("armed", true);
		this.plCtrls.anim.SetBool("Squashed", false);
		this.plCtrls.anim.SetBool("alive", true);
		base.transform.root.Find("Arms").GetComponent<Animator>().SetBool("alive", true);
		this.health = this.setHealth;
		this.updateHealthbar();
		base.GetComponent<Rigidbody2D>().isKinematic = false;
		base.transform.position = this.plCtrls.playerPos;
		yield return new WaitForSeconds(0.5f);
		this.invincible = true;
		yield return new WaitForSeconds(0.5f);
		this.plCtrls.canMove = true;
		this.killed = false;
		base.StartCoroutine(this.plTrans.flickerPlayer());
		this.invincible = false;
		this.plCtrls.canShoot = true;
		yield break;
	}

	// Token: 0x060000E6 RID: 230 RVA: 0x000098E4 File Offset: 0x00007AE4
	private void updateHealthbar()
	{
		base.transform.root.Find("_healthbar").GetComponent<PlayerHealthbar>().UpdateHealthBar(this.health);
	}

	// Token: 0x04000115 RID: 277
	public float health = 100f;

	// Token: 0x04000116 RID: 278
	[HideInInspector]
	public float setHealth;

	// Token: 0x04000117 RID: 279
	[HideInInspector]
	public bool invincible;

	// Token: 0x04000118 RID: 280
	[HideInInspector]
	public bool killed;

	// Token: 0x04000119 RID: 281
	private bool modeThrowLT = true;

	// Token: 0x0400011A RID: 282
	public AudioClip[] audioClips;

	// Token: 0x0400011B RID: 283
	private AudioSource[] audioSources;

	// Token: 0x0400011C RID: 284
	private Main mainScript;

	// Token: 0x0400011D RID: 285
	private PlayerControls plCtrls;

	// Token: 0x0400011E RID: 286
	private PlayerGames plGames;

	// Token: 0x0400011F RID: 287
	private PlayerTransform plTrans;
}
