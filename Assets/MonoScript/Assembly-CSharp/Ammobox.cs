﻿using System;
using System.Collections;
using UnityEngine;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;

// Token: 0x02000010 RID: 16
public class Ammobox : MonoBehaviour
{
	// Token: 0x0600004F RID: 79 RVA: 0x0000392C File Offset: 0x00001B2C
	private void Start()
	{
		this.startPos = base.transform.position;
		this.oldHealth = this.health;
	}

	// Token: 0x06000050 RID: 80 RVA: 0x0000395C File Offset: 0x00001B5C
	private void Update()
	{
	}

	// Token: 0x06000051 RID: 81 RVA: 0x00003960 File Offset: 0x00001B60
	private void OnTriggerEnter2D(Collider2D col)
	{
		if (col.tag == "Player")
		{
			base.transform.position = new Vector2(30f, 30f);
			base.GetComponent<AudioSource>().Play();
			PlayerControls component = col.gameObject.GetComponent<PlayerControls>();
			component.ammo += this.ammoAmount;
			GameObject.Find("GuiAmmo").GetComponent<GuiAmmo>().setAmmo();
			if (component.canShoot)
			{
				col.transform.Find("Arms").GetComponent<Animator>().SetBool("armed", true);
			}
			base.StartCoroutine("replaceBox");
		}
	}

	// Token: 0x06000052 RID: 82 RVA: 0x00003A18 File Offset: 0x00001C18
	public void damageObject(float damage)
	{
		this.health -= damage;
		base.transform.GetComponent<SpriteRenderer>().color = new Color(1f, 0f, 0f, 1f);
		base.StartCoroutine(this.defaultColor());
		if (this.health <= 0f)
		{
			GameObject.Find("_MainScript").GetComponent<MainFX>().explode(base.transform.position);
			base.transform.position = new Vector2(30f, 30f);
			base.StartCoroutine("replaceBox");
		}
	}

	// Token: 0x06000053 RID: 83 RVA: 0x00003AC8 File Offset: 0x00001CC8
	public IEnumerator replaceBox()
	{
		this.health = this.oldHealth;
		yield return new WaitForSeconds(this.respawnSec);
		if (this.ammoPos.Length > 0)
		{
			int randPos = Random.Range(0, this.ammoPos.Length);
			this.startPos = this.ammoPos[randPos];
		}
		base.transform.position = this.startPos;
		yield break;
	}

	// Token: 0x06000054 RID: 84 RVA: 0x00003AE4 File Offset: 0x00001CE4
	public IEnumerator defaultColor()
	{
		yield return new WaitForSeconds(0.1f);
		base.transform.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 1f);
		yield break;
	}

	// Token: 0x0400004A RID: 74
	public Vector2[] ammoPos;

	// Token: 0x0400004B RID: 75
	private Vector2 startPos;

	// Token: 0x0400004C RID: 76
	public float health = 50f;

	// Token: 0x0400004D RID: 77
	public float respawnSec = 20f;

	// Token: 0x0400004E RID: 78
	public int ammoAmount = 20;

	// Token: 0x0400004F RID: 79
	private float oldHealth;
}
