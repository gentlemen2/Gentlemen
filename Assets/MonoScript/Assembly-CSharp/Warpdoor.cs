﻿using System;
using System.Collections;
using UnityEngine;

// Token: 0x0200002A RID: 42
[RequireComponent(typeof(AudioSource))]
public class Warpdoor : MonoBehaviour
{
	// Token: 0x06000112 RID: 274 RVA: 0x0000A748 File Offset: 0x00008948
	private void Start()
	{
		this.spriteClosed = base.transform.GetComponent<SpriteRenderer>().sprite;
		if (this.exitDoor == null)
		{
			MonoBehaviour.print("Exit-Tür für " + base.name + " nicht gesetzt");
		}
		this.audioSources = new AudioSource[this.audioClips.Length];
		for (int i = 0; i < this.audioClips.Length; i++)
		{
			GameObject gameObject = new GameObject("AudioPlayer");
			gameObject.transform.parent = base.gameObject.transform;
			this.audioSources[i] = gameObject.AddComponent<AudioSource>() as AudioSource;
			this.audioSources[i].clip = this.audioClips[i];
		}
	}

	// Token: 0x06000114 RID: 276 RVA: 0x0000A814 File Offset: 0x00008A14
	private void OnTriggerStay2D(Collider2D col)
	{
		if (col.tag == "Player")
		{
			this.plCtrl = col.GetComponent<PlayerControls>();
			this.plLife = col.GetComponent<PlayerLife>();
			if (this.plCtrl.activate == 1 && !this.isBusy && !this.plLife.killed && this.plCtrl.grounded)
			{
				this.isBusy = true;
				this.actPlayer = col;
				this.yDiff = col.transform.position.y - base.transform.position.y;
				col.GetComponent<PlayerTransform>().makeInvisible(true);
				this.openDoor(true, base.transform);
				base.StartCoroutine("MovePlayer");
			}
		}
	}

	// Token: 0x06000115 RID: 277 RVA: 0x0000A8EC File Offset: 0x00008AEC
	public IEnumerator MovePlayer()
	{
		yield return new WaitForSeconds(0.3f);
		this.openDoor(false, base.transform);
		yield return new WaitForSeconds(this.exitTime);
		this.openDoor(true, this.exitDoor.transform);
		yield return new WaitForSeconds(0.5f);
		this.actPlayer.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
		this.actPlayer.transform.position = new Vector2(this.exitDoor.transform.position.x, this.exitDoor.transform.position.y + this.yDiff);
		this.actPlayer.GetComponent<PlayerTransform>().makeInvisible(false);
		this.exitDoor.transform.GetComponent<Warpdoor>().isBusy = true;
		yield return new WaitForSeconds(0.3f);
		this.openDoor(false, this.exitDoor.transform);
		yield return new WaitForSeconds(0.5f);
		this.isBusy = false;
		this.exitDoor.transform.GetComponent<Warpdoor>().isBusy = false;
	}

	// Token: 0x06000116 RID: 278 RVA: 0x0000A908 File Offset: 0x00008B08
	private void openDoor(bool makeOpen, Transform door)
	{
		if (makeOpen)
		{
			this.audioSources[0].Play();
			door.GetComponent<SpriteRenderer>().sprite = door.GetComponent<Warpdoor>().spriteOpen;
		}
		else
		{
			this.audioSources[1].Play();
			door.GetComponent<SpriteRenderer>().sprite = door.GetComponent<Warpdoor>().spriteClosed;
		}
	}

	// Token: 0x0400014E RID: 334
	public GameObject exitDoor;
	
	// Token: 0x0400014F RID: 335
	public float exitTime = 0.5f;

	// Token: 0x04000150 RID: 336
	private Collider2D actPlayer;

	// Token: 0x04000151 RID: 337
	private float yDiff;

	// Token: 0x04000152 RID: 338
	private bool isBusy;

	// Token: 0x04000153 RID: 339
	private Sprite spriteClosed;

	// Token: 0x04000154 RID: 340
	public Sprite spriteOpen;

	// Token: 0x04000155 RID: 341
	public AudioClip[] audioClips;

	// Token: 0x04000156 RID: 342
	private AudioSource[] audioSources;

	// Token: 0x04000157 RID: 343
	private PlayerControls plCtrl;

	// Token: 0x04000158 RID: 344
	private PlayerLife plLife;
}
