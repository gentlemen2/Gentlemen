﻿using System;
using System.Collections;
using UnityEngine;

// Token: 0x0200001B RID: 27
public class MainMenu : MonoBehaviour
{
	// Token: 0x060000A5 RID: 165 RVA: 0x000061E4 File Offset: 0x000043E4
	private void Update()
	{
	}

	// Token: 0x060000A6 RID: 166 RVA: 0x000061E8 File Offset: 0x000043E8
	private void Awake()
	{
	}

	// Token: 0x060000A7 RID: 167 RVA: 0x000061EC File Offset: 0x000043EC
	private void Start()
	{
		this.globalScript = GameObject.Find("_Globals").GetComponent<Globals>();
		this.globalScript.resetGoals();
		Time.timeScale = 1f;
		Color color = new Color(0.35f, 0.35f, 0.4f, 1f);
		this.H1.normal.textColor = color;
		this.H1.alignment = TextAnchor.MiddleCenter;
		this.H1.fontStyle = FontStyle.Normal;
		this.H1.fontSize = 35;
		this.H1.font = this.fontTitle;
		this.H2.normal.textColor = color;
		this.H2.fontStyle = FontStyle.Normal;
		this.H2.fontSize = 25;
		this.H2.font = this.fontTitle;
		this.copyGUIText.wordWrap = true;
		this.copyGUIText.normal.textColor = color;
		this.copyGUIText.font = this.fontCopy;
		this.actNrGamepads = Input.GetJoystickNames().Length;
	}

	// Token: 0x060000A8 RID: 168 RVA: 0x000062FC File Offset: 0x000044FC
	private void OnGUI()
	{
		GUI.skin = this.SmartSkin;
		if (this.pageNr == 0)
		{
			this.menuTitle = string.Empty;
			if (GUI.Button(new Rect(this.buttonCenter, (float)Screen.height * 0.4f, this.buttonWidth, this.buttonHeight), "Start Game"))
			{
				this.nextPage();
			}
			if (GUI.Button(new Rect(this.buttonCenter, (float)Screen.height * 0.48f, this.buttonWidth, this.buttonHeight), "Options"))
			{
				this.pageNr = -1;
			}
			if ((Application.platform == RuntimePlatform.OSXPlayer || Application.platform == RuntimePlatform.WindowsPlayer) && GUI.Button(new Rect(this.buttonCenter, (float)Screen.height * 0.56f, this.buttonWidth, this.buttonHeight), "Quit Game"))
			{
				Application.Quit();
			}
			GUI.Label(new Rect((float)Screen.width * 0.3f, (float)Screen.height * 0.65f, this.buttonWidth, this.buttonHeight), "Please give me feedback\nand stay tuned for updates:", this.H2);
			if (GUI.Button(new Rect((float)Screen.width * 0.2f, (float)Screen.height * 0.8f, this.buttonWidth * 0.5f, this.buttonHeight), "facebook"))
			{
				Application.ExternalEval("window.open('http://www.facebook.com/bambivalentcom/','_top')");
			}
			if (GUI.Button(new Rect((float)Screen.width * 0.34f, (float)Screen.height * 0.8f, this.buttonWidth * 0.7f, this.buttonHeight), "bambivalent.com"))
			{
				Application.ExternalEval("window.open('http://www.bambivalent.com/','_top')");
			}
			if (GUI.Button(new Rect((float)Screen.width * 0.52f, (float)Screen.height * 0.8f, this.buttonWidth * 0.5f, this.buttonHeight), "twitter"))
			{
				Application.ExternalEval("window.open('http://www.twitter.com/bambivalentcom/','_top')");
			}
			if (GUI.Button(new Rect((float)Screen.width * 0.65f, (float)Screen.height * 0.8f, this.buttonWidth * 0.5f, this.buttonHeight), "e-mail"))
			{
				Application.OpenURL("mailto:desk@bambivalent.com");
			}
			GUI.Label(new Rect((float)Screen.width * 0.05f, (float)Screen.height * 0.95f, this.buttonWidth, this.buttonHeight), "v. 0.11b");
		}
		if (this.pageNr == -1)
		{
			this.menuTitle = "Options";
			GUI.Label(new Rect(this.menuLeft, (float)Screen.height * 0.4f, this.buttonWidth, this.buttonHeight), "Melee Combat", this.H2);
			string text = ((!this.globalScript.g_modeMelee) ? "OFF" : "ON");
			if (GUI.Button(new Rect(this.menuTab2, (float)Screen.height * 0.4f, this.buttonWidth / 4f, this.buttonHeight), text))
			{
				this.globalScript.g_modeMelee = !this.globalScript.g_modeMelee;
			}
		}
		if (this.pageNr == 1)
		{
			this.menuTitle = "Players";
			if (GUI.Button(new Rect(this.buttonCenter, (float)Screen.height * 0.4f, this.buttonWidth, this.buttonHeight), "2 Players"))
			{
				this.globalScript.g_anzPlayer = 2;
				this.nextPage();
			}
			if (GUI.Button(new Rect(this.buttonCenter, (float)Screen.height * 0.48f, this.buttonWidth, this.buttonHeight), "3 Players"))
			{
				this.globalScript.g_anzPlayer = 3;
				this.nextPage();
			}
			if (GUI.Button(new Rect(this.buttonCenter, (float)Screen.height * 0.56f, this.buttonWidth, this.buttonHeight), "4 Players"))
			{
				this.globalScript.g_anzPlayer = 4;
				this.nextPage();
			}
		}
		if (this.pageNr == 2)
		{
			this.menuTitle = "Controls";
			this.checkGamepads();
		}
		if (this.pageNr == 3)
		{
			this.menuTitle = "Game Mode";
			this.globalScript.g_gameMode = GUI.SelectionGrid(new Rect(this.buttonCenter, (float)Screen.height * 0.4f, this.buttonWidth, 180f), this.globalScript.g_gameMode, this.selGameModes, 1);
			this.infotext = this.selGameModesInfo[this.globalScript.g_gameMode];
			this.nextButton();
		}
		if (this.pageNr == 4)
		{
			this.menuTitle = "Goals";
			if (this.globalScript.g_gameMode == 0)
			{
				GUI.Label(new Rect(this.menuLeft, (float)Screen.height * 0.4f, this.buttonWidth, this.buttonHeight), "Time", this.H2);
				if (this.globalScript.g_gameTime >= 5 && GUI.Button(new Rect(this.menuTab1, (float)Screen.height * 0.4f, this.buttonWidth / 4f, this.buttonHeight), "-"))
				{
					this.globalScript.g_gameTime -= 5;
				}
				if (GUI.Button(new Rect(this.menuTab3, (float)Screen.height * 0.4f, this.buttonWidth / 4f, this.buttonHeight), "+"))
				{
					this.globalScript.g_gameTime += 5;
				}
				string text2 = this.globalScript.g_gameTime.ToString();
				if (this.globalScript.g_gameTime <= 0)
				{
					text2 = "∞";
				}
				GUI.Label(new Rect(this.menuTab2, (float)Screen.height * 0.4f, this.buttonWidth, this.buttonHeight), text2, this.H2);
			}
			if (this.globalScript.g_gameMode == 0)
			{
				GUI.Label(new Rect(this.menuLeft, (float)Screen.height * 0.48f, this.buttonWidth, this.buttonHeight), "Kills", this.H2);
				if (this.globalScript.g_gameGoals[0] >= 5 && GUI.Button(new Rect(this.menuTab1, (float)Screen.height * 0.48f, this.buttonWidth / 4f, this.buttonHeight), "-"))
				{
					this.globalScript.g_gameGoals[0] -= 5;
				}
				if (GUI.Button(new Rect(this.menuTab3, (float)Screen.height * 0.48f, this.buttonWidth / 4f, this.buttonHeight), "+"))
				{
					this.globalScript.g_gameGoals[0] += 5;
				}
				string text3 = this.globalScript.g_gameGoals[0].ToString();
				if (this.globalScript.g_gameGoals[0] <= 0)
				{
					text3 = "∞";
				}
				GUI.Label(new Rect(this.menuTab2, (float)Screen.height * 0.48f, this.buttonWidth, this.buttonHeight), text3, this.H2);
			}
			if (this.globalScript.g_gameMode == 1)
			{
				this.globalScript.g_gameTime = 0;
				int num = 32;
				GUI.Label(new Rect(this.menuLeft, (float)Screen.height * 0.4f, this.buttonWidth, this.buttonHeight), "Data", this.H2);
				if (this.globalScript.g_gameGoals[1] > num && GUI.Button(new Rect(this.menuTab1, (float)Screen.height * 0.4f, this.buttonWidth / 4f, this.buttonHeight), "-"))
				{
					this.globalScript.g_gameGoals[1] -= num;
					this.globalScript.changeStatic(1, this.globalScript.g_gameGoals[1]);
				}
				if (GUI.Button(new Rect(this.menuTab3, (float)Screen.height * 0.4f, this.buttonWidth / 4f, this.buttonHeight), "+"))
				{
					this.globalScript.g_gameGoals[1] += num;
					this.globalScript.changeStatic(1, this.globalScript.g_gameGoals[1]);
				}
				string text4 = this.globalScript.g_gameGoals[1].ToString();
				GUI.Label(new Rect(this.menuTab2, (float)Screen.height * 0.4f, this.buttonWidth, this.buttonHeight), text4, this.H2);
				GUI.Label(new Rect(this.menuLeft, (float)Screen.height * 0.48f, this.buttonWidth, this.buttonHeight), "Throw Laptop", this.H2);
				string text5 = ((!this.globalScript.g_modeThrowLT) ? "OFF" : "ON");
				if (GUI.Button(new Rect(this.menuTab2, (float)Screen.height * 0.48f, this.buttonWidth / 4f, this.buttonHeight), text5))
				{
					this.globalScript.g_modeThrowLT = !this.globalScript.g_modeThrowLT;
				}
			}
			if (this.globalScript.g_gameMode == 2)
			{
				this.globalScript.g_gameTime = 0;
				int num2 = 5;
				GUI.Label(new Rect(this.menuLeft, (float)Screen.height * 0.4f, this.buttonWidth, this.buttonHeight), "Disks to win", this.H2);
				if (this.globalScript.g_gameGoals[2] > num2 && GUI.Button(new Rect(this.menuTab1, (float)Screen.height * 0.4f, this.buttonWidth / 4f, this.buttonHeight), "-"))
				{
					this.globalScript.g_gameGoals[2] -= num2;
				}
				if (GUI.Button(new Rect(this.menuTab3, (float)Screen.height * 0.4f, this.buttonWidth / 4f, this.buttonHeight), "+"))
				{
					this.globalScript.g_gameGoals[2] += num2;
				}
				string text6 = this.globalScript.g_gameGoals[2].ToString();
				GUI.Label(new Rect(this.menuTab2, (float)Screen.height * 0.4f, this.buttonWidth, this.buttonHeight), text6, this.H2);
				int num3 = 1;
				GUI.Label(new Rect(this.menuLeft, (float)Screen.height * 0.48f, this.buttonWidth, this.buttonHeight), "Max. discs", this.H2);
				if (this.globalScript.g_nrDisksSim > num3 && GUI.Button(new Rect(this.menuTab1, (float)Screen.height * 0.48f, this.buttonWidth / 4f, this.buttonHeight), "-"))
				{
					this.globalScript.g_nrDisksSim -= num3;
				}
				if (GUI.Button(new Rect(this.menuTab3, (float)Screen.height * 0.48f, this.buttonWidth / 4f, this.buttonHeight), "+"))
				{
					this.globalScript.g_nrDisksSim += num3;
				}
				string text7 = this.globalScript.g_nrDisksSim.ToString();
				GUI.Label(new Rect(this.menuTab2, (float)Screen.height * 0.48f, this.buttonWidth, this.buttonHeight), text7, this.H2);
				this.infotext = "Max. Discs = How many discs can be spawn at one time";
			}
			if (this.globalScript.g_gameMode == 3)
			{
				this.globalScript.g_gameTime = 0;
				int num4 = 5;
				GUI.Label(new Rect(this.menuLeft, (float)Screen.height * 0.48f, this.buttonWidth, this.buttonHeight), "Bombkills", this.H2);
				if (this.globalScript.g_gameGoals[3] <= -num4 && GUI.Button(new Rect(this.menuTab1, (float)Screen.height * 0.48f, this.buttonWidth / 4f, this.buttonHeight), "-"))
				{
					this.globalScript.g_gameGoals[3] += num4;
				}
				if (GUI.Button(new Rect(this.menuTab3, (float)Screen.height * 0.48f, this.buttonWidth / 4f, this.buttonHeight), "+"))
				{
					this.globalScript.g_gameGoals[3] -= num4;
				}
				string text8 = (-this.globalScript.g_gameGoals[3]).ToString();
				if (this.globalScript.g_gameGoals[3] >= 0)
				{
					text8 = "∞";
				}
				GUI.Label(new Rect(this.menuTab2, (float)Screen.height * 0.48f, this.buttonWidth, this.buttonHeight), text8, this.H2);
			}
			GUI.Label(new Rect(this.menuLeft, (float)Screen.height * 0.56f, this.buttonWidth, this.buttonHeight), "One Shot Killer", this.H2);
			string text9 = ((!this.globalScript.g_modeOneShot) ? "OFF" : "ON");
			if (GUI.Button(new Rect(this.menuTab2, (float)Screen.height * 0.56f, this.buttonWidth / 4f, this.buttonHeight), text9))
			{
				this.globalScript.g_modeOneShot = !this.globalScript.g_modeOneShot;
			}
			this.nextButton();
		}
		if (this.pageNr == 5)
		{
			this.menuTitle = "Select Level";
			if (GUI.Button(new Rect(this.buttonCenter, (float)Screen.height * 0.4f, this.buttonWidth, this.buttonHeight), "Hôtel Lys"))
			{
				this.globalScript.g_level = "Lvl_hotel";
				Application.LoadLevel("Lvl_hotel");
			}
			if (GUI.Button(new Rect(this.buttonCenter, (float)Screen.height * 0.48f, this.buttonWidth, this.buttonHeight), "Jump Factory"))
			{
				this.globalScript.g_level = "Level4";
				Application.LoadLevel("Level4");
			}
		}
		this.makeMenuTitle();
		this.makeInfo();
		this.backButton();
	}

	// Token: 0x060000A9 RID: 169 RVA: 0x000071B8 File Offset: 0x000053B8
	private void nextPage()
	{
		this.pageNr++;
		this.infotext = string.Empty;
	}

	// Token: 0x060000AA RID: 170 RVA: 0x000071D4 File Offset: 0x000053D4
	private void makeMenuTitle()
	{
		if (this.menuTitle != string.Empty)
		{
			GUI.Label(new Rect(0f, (float)Screen.height * 0.3f, (float)Screen.width, this.buttonHeight), this.menuTitle, this.H1);
		}
	}

	// Token: 0x060000AB RID: 171 RVA: 0x0000722C File Offset: 0x0000542C
	private void makeInfo()
	{
		if (this.infotext != string.Empty)
		{
			GUI.Box(new Rect((float)Screen.width / 3f, (float)Screen.height * 0.75f, (float)Screen.width / 3f, this.buttonHeight * 2f), string.Empty);
			GUI.Box(new Rect((float)Screen.width / 2.95f, (float)Screen.height * 0.755f, (float)Screen.width / 3.1f, this.buttonHeight * 1.95f), this.infotext, this.copyGUIText);
		}
	}

	// Token: 0x060000AC RID: 172 RVA: 0x000072D4 File Offset: 0x000054D4
	private void backButton()
	{
		if (!this.fireBtListener && this.pageNr != 0 && GUI.Button(new Rect((float)Screen.width * 0.1f, (float)Screen.height * 0.3f, this.buttonWidth, this.buttonHeight), this.backBt, string.Empty))
		{
			if (this.pageNr > 0)
			{
				this.pageNr--;
			}
			else
			{
				this.pageNr = 0;
			}
			this.infotext = string.Empty;
		}
	}

	// Token: 0x060000AD RID: 173 RVA: 0x0000736C File Offset: 0x0000556C
	private void nextButton()
	{
		if (!this.fireBtListener && GUI.Button(new Rect((float)Screen.width * 0.77f, (float)Screen.height * 0.3f, this.buttonWidth, this.buttonHeight), this.nextBt, string.Empty))
		{
			this.nextPage();
		}
	}

	// Token: 0x060000AE RID: 174 RVA: 0x000073D0 File Offset: 0x000055D0
	private void resetGamepadsButton()
	{
		if (!this.fireBtListener && GUI.Button(new Rect((float)Screen.width * 0.1f, (float)Screen.height * 0.8f, this.buttonWidth, this.buttonHeight), "Reset and check for gamepads"))
		{
			for (int i = 0; i < 5; i++)
			{
				for (int j = 0; j < 4; j++)
				{
					this.globalScript.g_Ctrls[i, j] = null;
				}
			}
			this.actPadID = 1;
			this.globalScript.g_nrGamepadsReady = 0;
			this.checkGamepads();
		}
	}

	// Token: 0x060000AF RID: 175 RVA: 0x00007470 File Offset: 0x00005670
	private void checkGamepads()
	{
		if (this.actNrGamepads > this.nrMaxGamepads)
		{
			this.infotext = "You have too many gamepads connected. Please disconnect " + (this.actNrGamepads - this.nrMaxGamepads) + " gamepads.";
		}
		else if (this.globalScript.g_nrGamepadsReady != this.actNrGamepads)
		{
			if (GUI.Button(new Rect((float)Screen.width * 0.4f, (float)Screen.height * 0.86f, this.buttonWidth, this.buttonHeight), "Play without gamepads"))
			{
				this.globalScript.g_nrGamepadsReady = this.actNrGamepads;
			}
			for (int i = 1; i <= this.nrAxes; i++)
			{
				for (int j = 1; j <= this.nrMaxGamepads; j++)
				{
					if (this.globalScript.g_Ctrls[this.actPadID, 0] == null && this.inputListener)
					{
						string text = "the NEXT";
						if (this.actPadID == 1)
						{
							text = "ANY";
						}
						this.infotext = string.Concat(new object[]
						{
							this.actNrGamepads,
							" gamepads found\nPlayer ",
							this.globalScript.g_nrGamepadsReady + 1,
							" Press RIGHT on ",
							text,
							" Gamepad"
						});
						if (Input.GetAxis(string.Concat(new object[] { "joy", j, "axis", i })) == 1f)
						{
							this.actAxisX = "axis" + i;
							this.inputPadID = j;
							MonoBehaviour.print(string.Concat(new object[] { "Pad ", this.inputPadID, " Horiz. Axis ", i, " pressed " }));
							this.infotext = string.Concat(new object[] { "Horiz: joy", this.inputPadID, "axis", i, " set" });
							if (this.globalScript.g_Ctrls[this.actPadID - 1, 0] != string.Concat(new object[] { "joy", this.inputPadID, "axis", i }) && this.globalScript.g_Ctrls[this.actPadID - 1, 1] != string.Concat(new object[] { "joy", this.inputPadID, "axis", i }))
							{
								this.globalScript.g_Ctrls[this.actPadID, 0] = string.Concat(new object[] { "joy", this.inputPadID, "axis", i });
							}
						}
					}
				}
				if (this.globalScript.g_Ctrls[this.actPadID, 0] != null && this.globalScript.g_Ctrls[this.actPadID, 1] == null)
				{
					this.infotext = string.Concat(new object[]
					{
						this.actNrGamepads,
						" gamepads found\nPlayer ",
						this.globalScript.g_nrGamepadsReady + 1,
						" Press DOWN on the SAME Gamepad"
					});
					if (Input.GetAxis(this.globalScript.g_Ctrls[this.actPadID, 0]) == 0f)
					{
						if (Input.GetAxis(string.Concat(new object[] { "joy", this.inputPadID, "axis", i })) == -1f)
						{
							this.globalScript.g_Ctrls[this.actPadID, 1] = string.Concat(new object[] { "joy", this.inputPadID, "axis", i });
							this.globalScript.g_Ctrls[this.actPadID, 4] = "yPos";
						}
						else if (Input.GetAxis(string.Concat(new object[] { "joy", this.inputPadID, "axis", i })) == 1f)
						{
							this.globalScript.g_Ctrls[this.actPadID, 1] = string.Concat(new object[] { "joy", this.inputPadID, "axis", i });
							this.globalScript.g_Ctrls[this.actPadID, 4] = "yNeg";
						}
					}
				}
			}
			if (this.globalScript.g_Ctrls[this.actPadID, 1] != null && this.globalScript.g_Ctrls[this.actPadID, 2] == null)
			{
				string text2 = this.globalScript.g_Ctrls[this.actPadID, 0].Replace(this.actAxisX, string.Empty) + "button0";
				this.globalScript.g_Ctrls[this.actPadID, 2] = text2;
				this.globalScript.g_Ctrls[this.actPadID, 3] = "Gamepad " + this.actPadID;
				this.globalScript.g_nrGamepadsReady++;
				this.globalScript.g_Ctrls[this.actPadID, 5] = this.inputPadID.ToString();
				this.actPadID++;
				base.StartCoroutine(this.pauseCtrlCheck());
			}
		}
		else
		{
			int num = 1;
			for (int k = 1; k <= 4; k++)
			{
				if (this.globalScript.g_Ctrls[k, 0] == null)
				{
					for (int l = 0; l < 5; l++)
					{
						this.globalScript.g_Ctrls[k, l] = this.globalScript.g_keySetup[num, l];
					}
					num++;
				}
			}
			this.resetGamepadsButton();
			this.showControls();
			this.nextButton();
		}
	}

	// Token: 0x060000B0 RID: 176 RVA: 0x00007B20 File Offset: 0x00005D20
	private void showControls()
	{
		int num = 0;
		int num2 = 0;
		string[] array = new string[] { "WASD\nLeft CTRL", "Arrow Keys\nReturn", "Num. Pad 8,4,5,6\nNum. Pad 0", "IJKL\nSpace" };
		this.infotext = string.Empty;
		if (!this.fireBtListener)
		{
			for (int i = 1; i <= this.globalScript.g_anzPlayer; i++)
			{
				float num3 = (float)this.globalScript.g_anzPlayer * 35f + (float)(Screen.width / this.globalScript.g_anzPlayer * i) * 0.55f;
				GUI.Label(new Rect(num3, (float)Screen.height * 0.4f, this.buttonWidth, this.buttonHeight), "Player " + i, this.H2);
				string text = this.globalScript.g_Ctrls[i, 3];
				string text2;
				if (text == "Keyboard")
				{
					text2 = array[num2];
					num2++;
				}
				else
				{
					text2 = text + "\nButton 1";
					num++;
				}
				int num4;
				if (this.globalScript.g_Ctrls[i, 4] == "yNeg")
				{
					num4 = -1;
				}
				else
				{
					num4 = 1;
				}
				GUI.Box(new Rect(num3 * 0.98f, (float)Screen.height * 0.48f, this.buttonWidth * 0.6f, this.buttonHeight * 1.5f), text2);
				if (Input.GetAxis(this.globalScript.g_Ctrls[i, 0]) < 0f)
				{
					this.infotext = "Player " + i + " move left";
				}
				if (Input.GetAxis(this.globalScript.g_Ctrls[i, 0]) > 0f)
				{
					this.infotext = "Player " + i + " move right";
				}
				if (Input.GetAxis(this.globalScript.g_Ctrls[i, 1]) * (float)num4 < 0f)
				{
					this.infotext = "Player " + i + " duck & lift down";
				}
				if (Input.GetAxis(this.globalScript.g_Ctrls[i, 1]) * (float)num4 > 0f)
				{
					this.infotext = "Player " + i + " activate & lift up";
				}
				if (Input.GetButton(this.globalScript.g_Ctrls[i, 2]))
				{
					this.infotext = "Player " + i + " fire";
				}
				if (text != "Keyboard" && GUI.Button(new Rect(num3 * 0.98f, (float)Screen.height * 0.58f, this.buttonWidth * 0.6f, this.buttonHeight), "Set Fire Button"))
				{
					this.fireBtListener = true;
					this.fireBtPlayer = i;
					this.fireBtIsGamepad = true;
				}
			}
			GUI.Label(new Rect((float)Screen.width * 0.4f, (float)Screen.height * 0.65f, this.buttonWidth, this.buttonHeight), "Press buttons for test!", this.H2);
		}
		else
		{
			string text3 = ((!this.fireBtIsGamepad) ? "press ANY KEY" : "press BUTTON 0 or 1 or A");
			this.infotext = string.Concat(new object[] { "Player ", this.fireBtPlayer, "\n", text3 });
			if (GUI.Button(new Rect((float)Screen.width * 0.4f, (float)Screen.height * 0.86f, this.buttonWidth, this.buttonHeight), "cancel"))
			{
				this.fireBtListener = false;
			}
			if (this.fireBtIsGamepad)
			{
				int num5 = int.Parse(this.globalScript.g_Ctrls[this.fireBtPlayer, 5]);
				for (int j = 0; j <= 1; j++)
				{
					if (Input.GetButton(string.Concat(new object[] { "joy", num5, "button", j })))
					{
						this.globalScript.g_Ctrls[this.fireBtPlayer, 2] = string.Concat(new object[] { "joy", num5, "button", j });
						this.fireBtListener = false;
					}
				}
			}
		}
	}

	// Token: 0x060000B1 RID: 177 RVA: 0x00007FC0 File Offset: 0x000061C0
	public IEnumerator pauseCtrlCheck()
	{
		this.inputListener = false;
		this.infotext = "Saving gamepad for player " + (this.actPadID - 1) + "...";
		yield return new WaitForSeconds(1f);
		this.inputListener = true;
		yield break;
	}

	// Token: 0x040000B5 RID: 181
	private Globals globalScript;

	// Token: 0x040000B6 RID: 182
	private GUIStyle H1 = new GUIStyle();

	// Token: 0x040000B7 RID: 183
	private GUIStyle H2 = new GUIStyle();

	// Token: 0x040000B8 RID: 184
	public Font fontCopy;

	// Token: 0x040000B9 RID: 185
	public Font fontTitle;

	// Token: 0x040000BA RID: 186
	private GUIStyle copyGUIText = new GUIStyle();

	// Token: 0x040000BB RID: 187
	public GUISkin SmartSkin;

	// Token: 0x040000BC RID: 188
	public Texture backBt;

	// Token: 0x040000BD RID: 189
	public Texture nextBt;

	// Token: 0x040000BE RID: 190
	private float menuLeft = (float)Screen.width * 0.3f;

	// Token: 0x040000BF RID: 191
	private float menuTab1 = (float)Screen.width * 0.5f;

	// Token: 0x040000C0 RID: 192
	private float menuTab2 = (float)Screen.width * 0.6f;

	// Token: 0x040000C1 RID: 193
	private float menuTab3 = (float)Screen.width * 0.67f;

	// Token: 0x040000C2 RID: 194
	private float buttonCenter = (float)Screen.width * 0.4f;

	// Token: 0x040000C3 RID: 195
	private float buttonWidth = (float)Screen.width * 0.2f;

	// Token: 0x040000C4 RID: 196
	private float buttonHeight = (float)Screen.height * 0.05f;

	// Token: 0x040000C5 RID: 197
	public int pageNr;

	// Token: 0x040000C6 RID: 198
	public string menuTitle = string.Empty;

	// Token: 0x040000C7 RID: 199
	public string infotext = string.Empty;

	// Token: 0x040000C8 RID: 200
	private string[] selGameModes = new string[] { "Death Match", "Capture the Laptop", "Disk Collectors", "Time Bomb" };

	// Token: 0x040000C9 RID: 201
	private string[] selGameModesInfo = new string[] { "Kill as much enemies as you can in this classic shootout.", "Your mission is to capture the laptop. As long as you carry it, you obtain and destroy data. Get as much data as possible before it is lost!", "Collect the data disks and bring them home to your terminal.", "Get rid of the timed bomb before it explodes." };

	// Token: 0x040000CA RID: 202
	private int actNrGamepads;

	// Token: 0x040000CB RID: 203
	private int inputPadID;

	// Token: 0x040000CC RID: 204
	private int actPadID = 1;

	// Token: 0x040000CD RID: 205
	private string actAxisX;

	// Token: 0x040000CE RID: 206
	private int nrMaxGamepads = 4;

	// Token: 0x040000CF RID: 207
	public int nrAxes = 7;

	// Token: 0x040000D0 RID: 208
	private bool inputListener = true;

	// Token: 0x040000D1 RID: 209
	private bool fireBtListener;

	// Token: 0x040000D2 RID: 210
	private int fireBtPlayer;

	// Token: 0x040000D3 RID: 211
	private bool fireBtIsGamepad;
}
