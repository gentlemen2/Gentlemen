﻿using System;
using UnityEngine;

// Token: 0x0200000F RID: 15
public class GuiTime : MonoBehaviour
{
	// Token: 0x0600004C RID: 76 RVA: 0x00003828 File Offset: 0x00001A28
	private void Start()
	{
		this.mainScript = GameObject.Find("_MainScript").GetComponent<Main>();
		base.GetComponent<GUIText>().text = "Time ∞";
	}

	// Token: 0x0600004D RID: 77 RVA: 0x00003850 File Offset: 0x00001A50
	private void Update()
	{
		int gameTime = this.mainScript.gameTime;
		if (gameTime > 0)
		{
			this.minutes = Mathf.Round((float)(gameTime / 60));
			this.seconds = (float)(gameTime % 60);
			this.secondOutput = string.Empty;
			if (this.seconds < 10f)
			{
				this.secondOutput = "0";
			}
			this.secondOutput += this.seconds.ToString();
			base.GetComponent<GUIText>().text = "Time " + this.minutes.ToString() + ":" + this.secondOutput;
		}
	}

	// Token: 0x04000046 RID: 70
	public Main mainScript;

	// Token: 0x04000047 RID: 71
	private float minutes;

	// Token: 0x04000048 RID: 72
	private float seconds;

	// Token: 0x04000049 RID: 73
	private string secondOutput;
}
