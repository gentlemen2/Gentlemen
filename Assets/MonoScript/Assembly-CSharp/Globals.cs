﻿using System;
using UnityEngine;
using Object = UnityEngine.Object;


// Token: 0x0200001A RID: 26
public class Globals : MonoBehaviour
{
	// Token: 0x0600009F RID: 159 RVA: 0x00005E6C File Offset: 0x0000406C
	public Globals()
	{
		string[,] array = new string[5, 6];
		array[0, 0] = string.Empty;
		array[0, 1] = string.Empty;
		array[0, 2] = string.Empty;
		array[0, 3] = string.Empty;
		array[0, 4] = string.Empty;
		array[0, 5] = string.Empty;
		array[1, 0] = "Horizontal";
		array[1, 1] = "Vertical";
		array[1, 2] = "Fire1";
		array[1, 3] = "Keyboard";
		array[1, 4] = "yPos";
		array[1, 5] = "0";
		array[2, 0] = "Horizontal2";
		array[2, 1] = "Vertical2";
		array[2, 2] = "Fire2";
		array[2, 3] = "Keyboard";
		array[2, 4] = "yPos";
		array[2, 5] = "0";
		array[3, 0] = "Horizontal3";
		array[3, 1] = "Vertical3";
		array[3, 2] = "Fire3";
		array[3, 3] = "Keyboard";
		array[3, 4] = "yPos";
		array[3, 5] = "0";
		array[4, 0] = "Horizontal4";
		array[4, 1] = "Vertical4";
		array[4, 2] = "Fire4";
		array[4, 3] = "Keyboard";
		array[4, 4] = "yPos";
		array[4, 5] = "0";
		this.g_keySetup = array;
		
	}

	// Token: 0x060000A0 RID: 160 RVA: 0x00006040 File Offset: 0x00004240
	private void Awake()
	{
		this.g_gameGoals = new int[] { 10, 256, 10, -10 };
		this.g_Ctrls = new string[5, 6];
		Object.DontDestroyOnLoad(this);
	}

	// Token: 0x060000A1 RID: 161 RVA: 0x00006078 File Offset: 0x00004278
	private void Update()
	{
	}

	// Token: 0x060000A2 RID: 162 RVA: 0x0000607C File Offset: 0x0000427C
	public void changeStatic(int gameGoal, int value)
	{
		this.g_goalDLs = this.g_gameGoals[1];
	}

	// Token: 0x060000A3 RID: 163 RVA: 0x0000608C File Offset: 0x0000428C
	public void resetGoals()
	{
		if (this.g_goalDLs > 0)
		{
			this.g_gameGoals[1] = this.g_goalDLs;
		}
	}

	// Token: 0x040000A7 RID: 167
	public int g_anzPlayer = 2;

	// Token: 0x040000A8 RID: 168
	public int g_gameMode;

	// Token: 0x040000A9 RID: 169
	public int g_gameTime = 5;

	// Token: 0x040000AA RID: 170
	public int[] g_gameGoals;

	// Token: 0x040000AB RID: 171
	public int g_goalDLs;

	// Token: 0x040000AC RID: 172
	public string g_level = string.Empty;

	// Token: 0x040000AD RID: 173
	public int g_nrDisksSim = 5;

	// Token: 0x040000AE RID: 174
	public Color[] g_plColors;

	// Token: 0x040000AF RID: 175
	public bool g_modeOneShot;

	// Token: 0x040000B0 RID: 176
	public bool g_modeMelee = true;

	// Token: 0x040000B1 RID: 177
	public bool g_modeThrowLT = true;

	// Token: 0x040000B2 RID: 178
	public int g_nrGamepadsReady;

	// Token: 0x040000B3 RID: 179
	public string[,] g_keySetup;

	// Token: 0x040000B4 RID: 180
	public string[,] g_Ctrls;
}
